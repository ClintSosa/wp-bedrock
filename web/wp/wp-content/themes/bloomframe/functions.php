<?php
/**
* bloomframe functions and definitions
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package bloomframe
*/
/*
* Specific content
*/
include(__DIR__ . '/includes/notices.php');
include(__DIR__ . '/includes/photos.php');
include(__DIR__ . '/includes/videos.php');
include(__DIR__ . '/includes/faq.php');

if (!function_exists('bloomframe_setup')) :

  function bloomframe_setup() {
    load_theme_textdomain('bloomframe', get_template_directory() . '/languages');

    // Add default posts and comments RSS feed links to head.
    add_theme_support('automatic-feed-links');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus(array(
      'menu-1' => esc_html__('Primary', 'bloomframe'),
      'menu-2' => esc_html__('Secondary', 'bloomframe')
    ));
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ));

    add_theme_support('custom-background', apply_filters('bloomframe_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    )));

    add_theme_support('customize-selective-refresh-widgets');
  }

endif;
add_action('after_setup_theme', 'bloomframe_setup');

function bloomframe_content_width() {
  $GLOBALS['content_width'] = apply_filters('bloomframe_content_width', 640);
}

add_action('after_setup_theme', 'bloomframe_content_width', 0);

/**
* Enqueue scripts and styles.
*/
function bloomframe_scripts() {
  wp_deregister_script('jquery');
  wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js', array(), '3.2.1', true);
  wp_enqueue_script('jquery');

  wp_enqueue_style('roboto', 'https://fonts.googleapis.com/css?family=Roboto:400,700');
  // wp_enqueue_style('bloomframe', get_template_directory_uri() . '/css/main.css');
  wp_enqueue_style('bloomframe', get_template_directory_uri() . '/css/main.min.css');
  wp_enqueue_script('bloomframe', get_template_directory_uri() . '/js/scripts.min.js', array('jquery'), '1.0.0', true);
  wp_enqueue_script('bloomframe-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);
  if (is_singular() && comments_open() && get_option('thread_comments')) {
    wp_enqueue_script('comment-reply');
  }
}

/**
* Ajax Url
*/
add_action('wp_head', 'myplugin_ajaxurl');
function myplugin_ajaxurl() {
  echo '<script type="text/javascript">
  var ajaxurl = "' . admin_url('admin-ajax.php') . '";
  </script>';
}


add_action('wp_enqueue_scripts', 'bloomframe_scripts');
require get_template_directory() . '/inc/custom-header.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/template-functions.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/jetpack.php';

/* Adminbar false */

function wpc_show_admin_bar() {
  return false;
}
add_filter('show_admin_bar', 'wpc_show_admin_bar');

/* Menu Custom */

function wp_nav_menu_no_ul() {
  $options = array(
    'echo' => false,
    'container' => false,
    'theme_location' => 'menu-1',
    'menu_id' => 'primary-menu'
  );

  $menu = wp_nav_menu($options);
  echo preg_replace(array('#^<ul[^>]*>#', '#</ul>$#'), '', $menu);
}

function wp_nav_menu2_no_ul() {
  $options = array(
    'echo' => false,
    'container' => false,
    'theme_location' => 'menu-2',
    'menu_id' => 'secondary-menu'
  );

  $menu = wp_nav_menu($options);
  echo preg_replace(array('#^<ul[^>]*>#', '#</ul>$#'), '', $menu);
}

/* Pagination */
function pagination($query) {

  //Pagination
  $big = 999999999; // need an unlikely integer
  $paginate_links = paginate_links( array(
    'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'current' => max( 1, get_query_var('paged') ),
    'total' => $query->max_num_pages,
    'prev_text' => __('Previous', 'bloomframe'),
    'next_text' => __('Next', 'bloomframe'),
    'prev_next' => false,
    'type' => 'list',
  ) );

  // Display the pagination if more than one page is found
  if( $paginate_links ){
    print '<ul class="pagination">';
    print $paginate_links;
    print '</ul>';
  }

}

/* Filtre page */

add_action('wp_ajax_filters_data', 'filters_data');
add_action('wp_ajax_nopriv_filters_data', 'filters_data');

function filters_data() {
  $choices = $_POST['choices'];
  $type = $_POST['typepost'];
  $meta_query = array('relation' => 'OR');
  foreach ($choices as $Key => $Value) {
    if (count($Value)) {
      foreach ($Value as $Inkey => $Invalue) {
        if($type == 'notice'){
          $meta_query[] = array('key' => $Key, 'value' => '"'.$Invalue.'"', 'compare' => 'LIKE');
        }else{
          $meta_query[] = array('key' => $Key, 'value' => $Invalue, 'compare' => '=');
        }
      }
    }
  }
  if(is_user_logged_in()){
    $args = array(
      'post_type' => $type.'s',
      'meta_query' => $meta_query);
  }else{
    $args = array(
      'post_type' => $type.'s',
      'meta_query' => $meta_query,
      'meta_key'		=> 'visible_pour_tous',
      'meta_value'	=> 'oui');
  }

  $query = new WP_Query($args);
  $nbpost = count($query->posts);
  $nbposttext = '<div class="list__results">';
  switch ($nbpost) {
    case '0':
      $nbposttext .= __('Aucun résultat', 'bloomframe');
      break;
    case '1':
      $nbposttext .= __('Un résultat', 'bloomframe');
      break;
    case ($nbpost > 1):
      $nbposttext .= $nbpost.' ';
      $nbposttext .= __('résultats', 'bloomframe');
      break;
    default:

      break;
  }
  $nbposttext .=  '</div>';
  if ($query->have_posts()) :
    print $nbposttext;
    while ($query->have_posts()): $query->the_post();
      get_template_part('template-parts/content-'.$type.'s', 'page');
    endwhile;
    wp_reset_query();
  else :
    wp_send_json($query->posts);
  endif;
  die();
}

/* File size */

function getSize($file) {
  if(!$file){
    return false;
  }
  $bytes = filesize($file);
  $s = array('b', 'Kb', 'Mb', 'Gb');
  $e = floor(log($bytes) / log(1024));
  return sprintf('%.2f ' . $s[$e], ($bytes / pow(1024, floor($e))));
}

/**
* Customize Wordpress Login Logo
* @url https://codex.wordpress.org/Customizing_the_Login_Form
*/
function my_login_logo() { ?>
  <style type="text/css">
    body.login{
      background: #f5f5f5;
    }
    #login h1 a, .login h1 a {
      background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/bloomframe-logo.svg);
      height:65px;
      width:320px;
      background-size: 240px auto;
      background-repeat: no-repeat;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


remove_action( 'wpcf7_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts' );
add_action( 'wpcf7_enqueue_scripts', 'wpcf7_recaptcha_enqueue_scripts_custom' );
function wpcf7_recaptcha_enqueue_scripts_custom() {
  $hl = !empty(ICL_LANGUAGE_CODE) ? ICL_LANGUAGE_CODE : 'en';
  $url = 'https://www.google.com/recaptcha/api.js';
  $url = add_query_arg( array(
    'hl' => $hl,
    'onload' => 'recaptchaCallback',
    'render' => 'explicit' ), $url );

  wp_register_script( 'google-recaptcha', $url, array(), '2.0', true );
}


//add_action( 'after_password_reset', 'bl_after_password_reset', 10, 2);
function bl_after_password_reset( $user, $new_pass){
  wp_redirect( home_url() ); exit;
}


add_filter( 'login_url', 'custom_url_login_after_resetpass', 10, 3);
function custom_url_login_after_resetpass($login_url, $redirect, $force_reauth ){
  //Modification de l'url du bouton connexion pour la page de confirmation de réinitialisation de mot de passe.
  if(!empty($_GET['action']) && $_GET['action'] == 'resetpass'){
    $login_url = home_url();
  }
  return  $login_url;
}
