<?php
/*
Template Name: FAQ
*/
$image = get_field('image_de_fond');
get_header(); ?>

<main class="main">
  <div class="main__container">
    <div class="main__content">

      <div class="main__hero">
        <div class="main__hero__bg" style="background-image:url(<?php echo $image['url'];?>)"></div>
      </div>

      <div class="main__intro">
        <div class="main__intro__container">
          <div class="main__intro__content">
            <?php
            if ( have_posts() ) :
              while ( have_posts() ) : the_post(); ?>
              <span class="main__intro__frame"></span>
              <h1 class="main__intro__heading">
                <span class="main__intro__title"><?php the_title(); ?></span>
              </h1>
              <div class="main__intro__inner">

              </div>
            <?php endwhile;
          endif; ?>
        </div>
      </div>
    </div>

    <section class="main__page">
      <div class="main__page__container">
        <div class="main__page__content">

          <div class="list faq">
            <?php
            if(is_user_logged_in()){
              $args = array(
                'post_type' => 'faqs',
                'posts_per_page' => 9,
                'paged'=>$paged);
              }else{
                $args = array(
                  'post_type' => 'faqs',
                  'posts_per_page' => 9,
                  'meta_key'		=> 'visible_pour_tous',
	                'meta_value'	=> 'oui',
                  'paged'=>$paged);
              }

              $loop = new WP_Query( $args );

              echo '<ul class="accordion" data-accordion>';
              while ( $loop->have_posts() ) : $loop->the_post();
              get_template_part( 'template-parts/content-faqs', 'page' );
            endwhile;
            echo '</ul>';
            pagination($loop);
            ?>
          </div>
        </div>
      </div>
    </section>

  </div>
</div>
</main>

<?php
get_footer();
