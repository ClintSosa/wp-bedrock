<?php

add_action('init', 'photos_init', 0);
add_filter('manage_photos_posts_columns', 'photos_colums', 5);
add_action('manage_photos_posts_custom_column', 'photos_level_column_td', 10, 2);
add_filter('manage_edit-photos_sortable_columns', 'photos_sortable_master_column');

function photos_init() {
// Creation de la page photos
    register_post_type('photos', array(
        'labels' => array(
            'name' => 'Photos',
            'singular_name' => 'Photos', 'post type singular name',
            'menu_name' => 'Photos', 'admin menu'),
            // 'public' => false, // it's not public, it shouldn't have it's own permalink, and so on
            'public' => true, // it's not public, it shouldn't have it's own permalink, and so on
            'publicly_queriable' => true, // you should be able to query it
            'show_ui' => true, // you should be able to edit it in wp-admin
            'exclude_from_search' => true, // you should exclude it from search results
            // 'show_in_nav_menus' => false, // you shouldn't be able to add it to menus
            'show_in_nav_menus' => true, // you shouldn't be able to add it to menus
        'has_archive' => false, // it shouldn't have archive page
        'rewrite' => false, // it shouldn't have rewrite rules
        'menu_icon' => 'dashicons-format-image',
        'supports' => [
            'title' => false,
            'editor' => false,
            'author' => false,
            'thumbnail' => false,
            'excerpt' => false,
            'trackbacks' => false,
            'custom-fields' => false,
            'comments' => false,
            'revisions' => false,
            'page-attributes' => false,
            'post-formats' => false,
    ]));
}

function photos_level_column_td($column, $post_id) {
    switch ($column) {
        case 'titre_de_la_photo':
            echo get_post_meta($post_id, 'titre_de_la_photo', true);
            break;
        case 'typologie':
            echo get_cat_name(get_post_meta($post_id, 'typologie', true));
            break;
        case 'photo':
          $image = get_post_meta($post_id, 'photo', true);
          echo wp_get_attachment_image($image, 'thumbnail');
            break;
    }
}

function photos_colums($columns) {
    $columns['titre_de_la_photo'] = 'Titre';
    $columns['typologie'] = 'Typologie';
    $columns['photo'] = 'Photo';
    unset($columns['title']);
    return $columns;
}

function photos_sortable_master_column() {
    return array(
          'titre_de_la_photo'=>'Titre',
          'typologie'=>'Typologie',
          'photo'=>'Photo'
    );
}
