<?php

add_action('init', 'faqs_init', 0);
add_filter('manage_faqs_posts_columns', 'faqs_colums', 5);
add_action('manage_faqs_posts_custom_column', 'faqs_level_column_td', 10, 2);
add_filter('manage_edit-faqs_sortable_columns', 'faqs_sortable_master_column');

function faqs_init() {
// Creation de la page faqs
    register_post_type('faqs', array(
        'labels' => array(
            'name' => 'FAQ',
            'singular_name' => 'FAQ', 'post type singular name',
            'menu_name' => 'FAQ', 'admin menu'),
        // 'public' => false, // it's not public, it shouldn't have it's own permalink, and so on
        'public' => true, // it's not public, it shouldn't have it's own permalink, and so on
        'publicly_queriable' => true, // you should be able to query it
        'show_ui' => true, // you should be able to edit it in wp-admin
        'exclude_from_search' => true, // you should exclude it from search results
        // 'show_in_nav_menus' => false, // you shouldn't be able to add it to menus
        'show_in_nav_menus' => true, // you shouldn't be able to add it to menus
        'has_archive' => false, // it shouldn't have archive page
        'rewrite' => false, // it shouldn't have rewrite rules
        'menu_icon' => 'dashicons-editor-help',
        'supports' => [
            'title' => false,
            'editor' => false,
            'author' => false,
            'thumbnail' => false,
            'excerpt' => false,
            'trackbacks' => false,
            'custom-fields' => false,
            'comments' => false,
            'revisions' => false,
            'page-attributes' => false,
            'post-formats' => false,
    ]));
}

function faqs_level_column_td($column, $post_id) {
    switch ($column) {
        case 'question':
            echo get_post_meta($post_id, 'question', true);
            break;
    }
}

function faqs_colums($columns) {
    $columns['question'] = 'Question';
    unset($columns['title']);
    return $columns;
}

function faqs_sortable_master_column() {
    return array(
          'question'=>'Question'
    );
}
