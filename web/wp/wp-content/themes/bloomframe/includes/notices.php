<?php

add_action('init', 'notices_init', 0);
add_filter('manage_notices_posts_columns', 'notices_colums', 5);
add_action('manage_notices_posts_custom_column', 'notices_level_column_td', 10, 2);
add_filter('manage_edit-notices_sortable_columns', 'notices_sortable_master_column');

function notices_init() {
// Creation de la page notices
    register_post_type('notices', array(
        'labels' => array(
            'name' => 'Notices',
            'singular_name' => 'Notices', 'post type singular name',
            'menu_name' => 'Notices', 'admin menu'),
            // 'public' => false, // it's not public, it shouldn't have it's own permalink, and so on
            'public' => true, // it's not public, it shouldn't have it's own permalink, and so on
            'publicly_queriable' => true, // you should be able to query it
            'show_ui' => true, // you should be able to edit it in wp-admin
            'exclude_from_search' => true, // you should exclude it from search results
            // 'show_in_nav_menus' => false, // you shouldn't be able to add it to menus
            'show_in_nav_menus' => true, // you shouldn't be able to add it to menus
        'has_archive' => false, // it shouldn't have archive page
        'rewrite' => false, // it shouldn't have rewrite rules
        'menu_icon' => 'dashicons-format-aside',
        'supports' => [
            'title' => false,
            'editor' => false,
            'author' => false,
            'thumbnail' => false,
            'excerpt' => false,
            'trackbacks' => false,
            'custom-fields' => false,
            'comments' => false,
            'revisions' => false,
            'page-attributes' => false,
            'post-formats' => false,
    ]));
}

function notices_level_column_td($column, $post_id) {
    switch ($column) {
        case 'titre_du_document':
            echo get_post_meta($post_id, 'titre_du_document', true);
            break;
        case 'typologie':
            foreach (get_post_meta($post_id, 'typologie', true) as $typologie):
                    echo ' ' . get_cat_name($typologie);
            endforeach;
            break;
        case 'poids':
            echo get_post_meta($post_id, 'poids', true);
            break;
        case 'type_dextension':
            echo get_post_meta($post_id, 'type_dextension', true);
            break;
        case 'fichier_notice':
            $fichier = basename(get_attached_file(get_post_meta($post_id, 'fichier_notice', true)));
            echo $fichier;
            break;
    }
}

function notices_colums($columns) {
    $columns['titre_du_document'] = 'Titre';
    $columns['typologie'] = 'Typologie';
    $columns['poids'] = 'Poids';
    $columns['type_dextension'] = 'Type de fichier';
    $columns['fichier_notice'] = 'Fichier';
    unset($columns['title']);
    return $columns;
}

function notices_sortable_master_column() {
    return array(
          'titre_du_document'=>'Titre',
          'typologie'=>'Typologie',
          'poids'=>'Poids',
          'type_dextension'=>'Extension',
          'fichier_notice'=>'Fichier'
    );
}
