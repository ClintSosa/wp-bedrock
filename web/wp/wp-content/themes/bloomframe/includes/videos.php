<?php

add_action('init', 'videos_init', 0);
add_filter('manage_videos_posts_columns', 'videos_colums', 5);
add_action('manage_videos_posts_custom_column', 'videos_level_column_td', 10, 2);
add_filter('manage_edit-videos_sortable_columns', 'videos_sortable_master_column');

function videos_init() {
// Creation de la page videos
    register_post_type('videos', array(
        'labels' => array(
            'name' => 'Videos',
            'singular_name' => 'Videos', 'post type singular name',
            'menu_name' => 'Videos', 'admin menu'),
            // 'public' => false, // it's not public, it shouldn't have it's own permalink, and so on
            'public' => true, // it's not public, it shouldn't have it's own permalink, and so on
            'publicly_queriable' => true, // you should be able to query it
            'show_ui' => true, // you should be able to edit it in wp-admin
            'exclude_from_search' => true, // you should exclude it from search results
            // 'show_in_nav_menus' => false, // you shouldn't be able to add it to menus
            'show_in_nav_menus' => true, // you shouldn't be able to add it to menus
        'has_archive' => false, // it shouldn't have archive page
        'rewrite' => false, // it shouldn't have rewrite rules
        'menu_icon' => 'dashicons-format-video',
        'supports' => [
            'title' => false,
            'editor' => false,
            'author' => false,
            'thumbnail' => false,
            'excerpt' => false,
            'trackbacks' => false,
            'custom-fields' => false,
            'comments' => false,
            'revisions' => false,
            'page-attributes' => false,
            'post-formats' => false,
    ]));
}

function videos_level_column_td($column, $post_id) {
    switch ($column) {
        case 'titre_de_la_video':
            echo get_post_meta($post_id, 'titre_de_la_video', true);
            break;
        case 'typologie':
            echo get_cat_name(get_post_meta($post_id, 'typologie', true));
            break;
        case 'video':
          $fichier_video_mp4 = get_post_meta($post_id,'video', true);
          $fichier_video_ogv = get_post_meta($post_id,'video_2', true);
          if( !empty($fichier_video_mp4) ){
            echo '*.mp4 ';
          }
          if( !empty($fichier_video_ogv) ){
            echo '*.ogv';
          }

          break;
    }
}

function videos_colums($columns) {
    $columns['titre_de_la_video'] = 'Titre';
    $columns['typologie'] = 'Typologie';
    $columns['video'] = 'video';
    unset($columns['title']);
    return $columns;
}

function videos_sortable_master_column() {
    return array(
          'titre_de_la_video'=>'Titre',
          'typologie'=>'Typologie',
          'video'=>'video'
    );
}
