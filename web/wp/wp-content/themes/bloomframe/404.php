<?php
/**
* The template for displaying 404 pages (not found)
*
* @link https://codex.wordpress.org/Creating_an_Error_404_Page
*
* @package bloomframe
*/

get_header(); ?>

<main class="main">
	<div class="main__container">
		<div class="main__content 404">
			404
		</div>
	</div>
</main>

<?php
get_footer();
