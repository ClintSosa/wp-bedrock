<?php
/**
* The header for our theme
*
* This is the template that displays all of the <head> section and everything up until <div id="content">
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package bloomframe
*/
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo bloginfo('home'); ?>/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo bloginfo('home'); ?>/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo bloginfo('home'); ?>/favicon-16x16.png">
  <link rel="manifest" href="<?php echo bloginfo('home'); ?>/manifest.json">
  <link rel="mask-icon" href="<?php echo bloginfo('home'); ?>/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

  <header class="header" role="banner">

    <div class="header__container">
      <div class="header__content">

        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header__logo"><img src="<?php echo get_template_directory_uri(); ?>/img/bloomframe-logo.svg" alt="<?php echo bloginfo('name') ?>"></a>

        <nav id="site-navigation" class="header__nav" role="navigation">
          <div class="header__nav__bg"></div>
          <ul id="primary-menu" class="header__menu">
            <?php
            wp_nav_menu_no_ul();
            if (is_user_logged_in()) {
              echo '<li class="logout"><a href="' . wp_logout_url(site_url('/')) . '">';
              _e('Déconnexion', 'bloomframe');
              echo '</a></li>';
            }
            ?>
          </ul>
        </nav>

        <button type="button" class="header__open" id="trigger-menu"><span><?php _e('Menu') ?></span> <i></i></button>

      </div>
    </div>
  </header>
