<?php
$post_id = get_the_ID();
$titre_du_document = get_field('titre_de_la_video', $post_id);
$typologie = get_field('typologie', $post_id);
$resume = get_field('resume', $post_id);
$duree = get_field('duree', $post_id);
$fichier_video_mp4 = get_field('video', $post_id);
$fichier_video_ogv = get_field('video_2', $post_id);
$images = get_field('images_fond', $post_id, true);
?>
<div class="list__item video">
  <div class="list__item__thumb">
    <?php
      $url_tumb = ' style="background-image : url('.$images.')"';
     ?>
    <div class="list__item__backdrop js-video-open" data-open="video-<?php echo $post_id; ?>" <?php echo $url_tumb; ?>></div>
    <div class="responsive-embed">
      <video>
        <source src="<?php echo $fichier_video_mp4["url"]; ?>" type="video/mp4" />
        <source src="<?php echo $fichier_video_ogv["url"]; ?>" type="video/ogg" />
      </video>
    </div>
  </div>

  <h2 class="list__item__title"><?php echo $titre_du_document; ?></h2>
  <div class="breadcrumb"><?php echo get_cat_name($typologie); ?></div>
  <div class="list__item__desc"><?php echo $resume; ?></div>
  <div class="list__item__meta">
    <div class="list__item__duration"><?php echo $duree; ?></div>
  </div>
</div>

<div class="reveal small" id="video-<?php echo get_the_ID() ?>" data-reveal>
  <div class="responsive-embed">
    <video preload="true" controls>
      <source src="<?php echo $fichier_video_mp4["url"]; ?>" type="video/mp4" />
        <source src="<?php echo $fichier_video_ogv["url"]; ?>" type="video/ogg" />
    </video>
  </div>
  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
