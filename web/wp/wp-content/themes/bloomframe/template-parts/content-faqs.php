<?php
$post_id = get_the_ID();
$question = get_field('question', $post_id);
$reponse = get_field('reponse', $post_id);

echo '<li class="accordion-item" data-accordion-item>';
  // echo '<a href="#" class="accordion-title">'.$question.'<span>'._("Voir la réponse").'</span></a>';
  echo '<a href="#" class="accordion-title">'.$question.'</a>';
  echo '<div class="accordion-content" data-tab-content>';
    echo $reponse;
  echo '</div>';
echo '</li>';
