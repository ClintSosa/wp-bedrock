<?php

$post_id = get_the_ID();
$titre_du_document = get_field('titre_du_document', $post_id);
$typologies = get_field('typologie', $post_id);
$resume = get_field('resume', $post_id);
$fichier_notice = get_field('fichier_notice', $post_id);

if($fichier_notice) :
  $url = wp_get_attachment_url($fichier_notice);
  $file_path = get_attached_file($fichier_notice);
  $path_parts = pathinfo($file_path);
  $type_dextension = $path_parts['extension'];
  $poids = getSize( $file_path );
  ?>
  <article class="list__item '.$type_dextension.' notice">
    <div class="list__item__content">
      <h2 class="list__item__title"><?php print $titre_du_document;?></h2>
        <?php if ($typologies): ?>
          <div class="breadcrumb">
            <?php
            $i = 0;
            foreach ($typologies as $typologie):
              if ($i == 0) :
                print get_cat_name($typologie);
              else :
                print '<span> &gt; <span>' . get_cat_name($typologie);
              endif;
              $i++;
            endforeach; ?>
          </div>
          <?php
        endif; ?>

        <div class="list__item__desc">
          <?php print $resume; ?>
          <div class="list__item__size"><?php print $poids . ' - ' . $type_dextension;?></div>
        </div>
    </div>
    <div class="list__item__link"><a target="_blank" download href="<?php print $url;?>" class="button"><?php print __('Download', 'bloomframe');?></a></div>
  </article>
  <?php
endif; ?>
