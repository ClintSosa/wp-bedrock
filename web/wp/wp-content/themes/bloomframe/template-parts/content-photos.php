<?php
$post_id = get_the_ID();
$titre_du_document = get_field('titre_de_la_photo', $post_id);
$typologie = get_field('typologie', $post_id);
$resume = get_field('resume', $post_id);
$fichier_photo = get_field('photo', $post_id);
?>
<div class="list__item photo">
  <div class="list__item__thumb"><a href="<?php echo $fichier_photo['url']; ?>" data-lightbox="light<?php echo $post_id; ?>" data-title="<?php echo !empty($fichier_photo['title']) ? $fichier_photo['title'] : ''; ?>"><img src="<?php echo $fichier_photo['url']; ?>" alt="<?php echo !empty($fichier_photo['title']) ? $fichier_photo['title'] : ''; ?>" /></a></div>
  <h2 class="list__item__title"><?php echo $titre_du_document; ?></h2>
  <div class="breadcrumb"><?php echo get_cat_name($typologie); ?></div>
  <div class="list__item__desc"><?php echo $resume; ?></div>
</div>
