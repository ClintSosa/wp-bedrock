<?php $image = get_field('image_de_fond'); ?>
<div class="main__hero">
  <div class="main__hero__bg" style="background-image:url(<?php echo $image['url'];?>)"></div>
</div>
<div class="main__intro">
  <div class="main__intro__container">
    <div class="main__intro__content">
      <?php
      if ( have_posts() ) :
        while ( have_posts() ) : the_post(); ?>
        <span class="main__intro__frame"></span>
        <h1 class="main__intro__heading">
          <!-- <span class="main__intro__subtitle"><?php //_e('Bienvenue sur la ', 'bloomframe'); ?></span> -->
          <span class="main__intro__title"><?php the_title(); ?></span>
        </h1>
        <div class="main__intro__inner">
          <?php if (!is_user_logged_in()) :
            the_content();
          endif; ?>
        </div>
        <?php
      endwhile;
    endif; ?>
  </div>
</div>
</div>
<?php
if (!is_user_logged_in()) :
  ?>
  <!-- Zone pour connaitre le type de visiteur -->
  <!-- <div>
    Vous êtes ?
  </div> -->

  <!-- ZOne pour se connecter -->
  <div class="home__login">
    <div class="home__login__container">
      <div class="home__login__content">
        <div class="home__login__title"><?php _e('Espace connexion', 'bloomframe'); ?></div>
        <form  class="home__login__form" name="login-form" id="login-form" action="<?php echo wp_login_url( get_permalink() ); ?>" method="post" autocomplete="off">
          <div class="form__control">
            <label>
              <span class="form__label"><?php _e('Identifiant', 'bloomframe'); ?></span>
              <input name="log" class="form__input" type="text" id="user-login" value="" size="20" type="text">
            </label>
          </div>
          <div class="form__control">
            <label>
              <span class="form__label"><?php _e('Mot de passe', 'bloomframe'); ?></span>
              <input name="pwd" id="user-pass" class="input" value="" size="20" type="password">
            </label>
          </div>

          <div class="form__control form__footer">
            <input name="wp-submit" id="wp-submit" class="button button-primary" value="<?php _e('Se connecter', 'bloomframe'); ?>" type="submit">
            <a href="<?php echo wp_lostpassword_url(get_permalink()); ?>" title="<?php _e('Mot de passe oublié ?', 'bloomframe'); ?>"><?php _e('Mot de passe oublié ?', 'bloomframe'); ?></a>
            <input name="redirect_to" value="<?php echo home_url('/') ?>" type="hidden">
          </div>
        </form>

        <?php $icl_object_id = icl_object_id(94, 'page', true); ?>
        <a href="<?php print get_permalink($icl_object_id); ?>"><?php _e("Vous n'avez pas encore de compte ?", 'bloomframe'); ?></a>
      </div>
    </div>
  </div>
  <?php
else:
  ?>
  <div class="home__connected">
    <div class="home__connected__container">
      <div class="home__connected__content">

        <div class="home__connected__item notices">
          <a href="<?php echo get_permalink(19); ?>" class="home__connected__item__link">
            <span class="home__connected__item__backdrop"></span>
            <span class="home__connected__item__frame"></span>
            <h2 class="home__connected__item__title"><?php _e('Notices', 'bloomframe'); ?></h2>
            <div class="home__connected__item__desc"><?php the_field('texte_notices'); ?></div>
          </a>
        </div>

        <div class="home__connected__item photos">
          <a href="<?php echo get_permalink(41); ?>" class="home__connected__item__link">
            <span class="home__connected__item__backdrop"></span>
            <span class="home__connected__item__frame"></span>
            <h2 class="home__connected__item__title"><?php _e('Photos', 'bloomframe'); ?></h2>
            <div class="home__connected__item__desc"><?php the_field('texte_photos'); ?></div>
          </a>
        </div>

        <div class="home__connected__item videos">
          <a href="<?php echo get_permalink(80); ?>" class="home__connected__item__link">
            <span class="home__connected__item__backdrop"></span>
            <span class="home__connected__item__frame"></span>
            <h2 class="home__connected__item__title"><?php _e('Videos', 'bloomframe'); ?></h2>
            <div class="home__connected__item__desc"><?php the_field('texte_videos'); ?></div>
          </a>
        </div>

        <div class="home__connected__item faq">
          <a href="<?php echo get_permalink(91); ?>" class="home__connected__item__link">
            <span class="home__connected__item__backdrop"></span>
            <span class="home__connected__item__frame"></span>
            <h2 class="home__connected__item__title"><?php _e('FAQ', 'bloomframe'); ?></h2>
            <div class="home__connected__item__desc"><?php the_field('texte_faq'); ?></div>
          </a>
        </div>
      </div>
    </div>
  </div>

  <?php
endif;
