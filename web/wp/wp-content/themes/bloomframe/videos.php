<?php
/*
Template Name: Videos
*/
$image = get_field('image_de_fond');
get_header(); ?>

<main class="main">
  <div class="main__container">
    <div class="main__content">

      <div class="main__hero">
        <div class="main__hero__bg" style="background-image:url(<?php echo $image['url'];?>)"></div>
      </div>

      <div class="main__intro">
        <div class="main__intro__container">
          <div class="main__intro__content">
            <?php
            if ( have_posts() ) :
              while ( have_posts() ) : the_post(); ?>
              <span class="main__intro__frame"></span>
              <h1 class="main__intro__heading">
                <span class="main__intro__title"><?php the_title(); ?></span>
              </h1>
              <div class="main__intro__outer"><?php _e('Je souhaite voir les vidéos...', 'bloomframe'); ?></div>
              <div class="main__intro__inner">
                <div class="filters" id="filters">
                  <?php
                  $taxonomies = get_terms(array('taxonomy' =>'category'));

                  foreach ($taxonomies as $taxonomie) {
                    echo '<div class="filters__item">';
                    echo '<input type="checkbox" name="typologie" value="'.$taxonomie->term_id.'" class="typologie" id="filter'.$taxonomie->term_id.'">';
                    echo '<label for="filter'.$taxonomie->term_id.'">'.__($taxonomie->name, 'bloomframe').'</label>';
                    echo '</div>';
                  }
                   ?>
                  <input type="hidden" name="type" id="typepost" value="video"/>
                </div>
              </div>
            <?php endwhile;
          endif; ?>
        </div>
      </div>
    </div>

    <section class="main__page">
      <div class="main__page__container">
        <div class="main__page__content">

          <div class="list videos">
            <?php
            if(is_user_logged_in()){
              $args = array(
                'post_type' => 'videos',
                'posts_per_page' => 9,
                'paged'=>$paged);
              }else{
                $args = array(
                  'post_type' => 'videos',
                  'posts_per_page' => 9,
                  'meta_key'		=> 'visible_pour_tous',
                  'meta_value'	=> 'oui',
                  'paged'=>$paged);
              }

              $loop = new WP_Query( $args );
              $nbpost = $loop->found_posts;
              echo '<div class="list__results">';
              switch ($nbpost) {
                case '0':
                  _e('Aucun résultat', 'bloomframe');
                  break;
                case '1':
                  _e('Un résultat', 'bloomframe');
                  break;
                case ($nbpost > 1):
                  echo $nbpost.' ';
                  _e('résultats', 'bloomframe');
                  break;
                default:

                  break;
              }
              echo '</div>';

              while ( $loop->have_posts() ) : $loop->the_post();
              get_template_part( 'template-parts/content-videos', 'page' );
            endwhile;

            pagination($loop);
            ?>
          </div>
        </div>
      </section>

    </div>
  </div>
</main>

<?php
get_footer();
