<?php
/*
Template Name: Page de connexion
*/
get_header(); ?>

<main class="main">
	<div class="main__container">
		<div class="main__content">

			<?php get_template_part( 'template-parts/content-login', 'page' ); ?>

		</div>
	</div>
</main>

<?php
get_footer();
