<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages
* and that other 'pages' on your WordPress site may use a
* different template.
*
* @link https://codex.wordpress.org/Template_Hierarchy
*
* @package bloomframe
*/
$image = get_field('image_de_fond');
get_header(); ?>

<main class="main">
	<div class="main__container">
		<div class="main__content">

			<div class="main__hero">
				<div class="main__hero__bg" style="background-image:url(<?php echo $image['url'];?>)"></div>
			</div>

			<div class="main__intro">
				<div class="main__intro__container">
					<div class="main__intro__content">
						<?php
						if ( have_posts() ) :
							while ( have_posts() ) : the_post(); ?>
							<span class="main__intro__frame"></span>
							<h1 class="main__intro__heading">
								<span class="main__intro__title"><?php the_title(); ?></span>
							</h1>
							<div class="main__intro__inner"></div>
						</div>
					</div>
				</div>

				<section class="main__page">
					<div class="main__page__container">
						<div class="main__page__content">

							<?php
							get_template_part( 'template-parts/content', 'page' );

						endwhile; // End of the loop.
					endif;?>
				</div>
			</div>
		</section>

	</div>
</div>
</main>

<?php
get_footer();
