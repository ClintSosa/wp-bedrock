<?php
/**
* The template for displaying the footer
*
* Contains the closing of the #content div and all content after.
*
* @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
*
* @package bloomframe
*/
?>

<footer class="footer" id="colophon" role="contentinfo">
	<div class="footer__container">
		<div class="footer__content">
			<div class="footer__nav">
				<ul id="secondary-menu" class="footer__menu">
					<?php wp_nav_menu2_no_ul(); ?>
				</ul>
			</div>
			<div class="footer__credit">
				<a href="#"><img src="<?php echo get_template_directory_uri() ?>/img/bloomframe-kawneer-logo.png" alt="Kawneer"></a>
			</div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
