<?php
/**
 * Template Name: Page formulaire
 *
 */
?>

<?php
get_header(); ?>



<?php
      while ( have_posts() ) : the_post();
          $post_id = $post->ID;

      endwhile; // End of the loop.
      ?>
<?php $image = get_field('visuel', $post_id);
if(!empty($image)) :
    //size : (thumbnail, medium, large, full or custom size)
    $size = 'full'; ?>

    <section class="header-splash" style="background-image: url('<?php print wp_get_attachment_url($image['ID'], $size); ?>')">

        <div container>
            <div grid="bottom">
                <h1><?php print get_the_title($post_id);?></h1>
            </div>
        </div>


    </section>

    <?php
endif; ?>

    <section>
        <div container>
            <?php print choosit_wp_breadcrumb();?>
        </div>
    </section>


    <section id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div container>
            <div grid="">
                <div class="page--content">
                    <?php the_content();?>
                </div>

                <div class="page--sidebar">
                    <?php if(get_field('informations')) : ?>
                        <div class="page--sidebar_nav">
                            <?php print get_field('informations');?>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>
    </section>


<?php
get_footer();
