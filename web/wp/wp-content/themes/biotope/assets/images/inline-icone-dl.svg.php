
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 127.559 127.559" style="enable-background:new 0 0 127.559 127.559;" xml:space="preserve">
<path d="M70.498,45.03v2.656h17.344v45.312H39.717V47.686h17.344V45.03h-20v50.781h53.438V45.03H70.498z M52.061,63.623
	l-1.875,1.875L63.78,79.092l13.594-13.594l-1.875-1.875L65.186,73.936V31.748h-2.812v42.188L52.061,63.623z"/>
</svg>
