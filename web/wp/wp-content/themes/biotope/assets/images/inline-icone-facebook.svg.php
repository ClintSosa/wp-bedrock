
<svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 127.559 127.559" style="enable-background:new 0 0 127.559 127.559;" xml:space="preserve">
<path d="M68.78,53.779h10l-1.25,10h-8.75v30H55.811v-30H48.78v-10h7.031V47.06c0-4.375,1.094-7.682,3.281-9.922
	c2.188-2.239,5.834-3.359,10.938-3.359h8.75v10h-5.312c-1.979,0-3.255,0.312-3.828,0.938c-0.574,0.624-0.86,1.666-0.86,3.124V53.779
	z"/>
</svg>
