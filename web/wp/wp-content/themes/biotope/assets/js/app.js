/**
 * Project: Biotope
 * Version: 0.0.1
 */

/**
 * Project: Biotope
 * Version: 0.0.1
 */

"use strict";

var mapGoogle, trafficLayer;

function initMap() {

    if (PHP_DATA.markers) {
        var markers = PHP_DATA.markers;
    } else {
        return false;
    }

    var mapOptions = {
        zoom: 2,
        center: new google.maps.LatLng(markers[0].latitude, markers[0].longitude),
        //center: new google.maps.LatLng('48.8555996', '2.3484359'),
        scrollwheel: false,
        zoomControl: true,
        draggable: true,
        mapTypeId: 'hybrid' //roadmap, satellite, terrain
    };

    mapGoogle = new google.maps.Map(document.getElementById('map'), mapOptions);

    // var icon = {
    //     path: "M0-165c-27.618 0-50 21.966-50 49.054C-50-88.849 0 0 0 0s50-88.849 50-115.946C50-143.034 27.605-165 0-165z",
    //     fillColor: '#0091a6',
    //     fillOpacity: .8,
    //     anchor: new google.maps.Point(0, 0),
    //     strokeWeight: 0,
    //     scale: 1 / 4
    // };
    //icon = Drupal.settings.base_image_dir + '/markers/' + markername+'.png';


    var lat_min = 90.0;
    var lng_min = 180.0;
    var lat_max = -90.0;
    var lng_max = -180.0;
    jQuery.each(markers, function (key, value) {

        var latitude = parseFloat(value.latitude);
        var longitude = parseFloat(value.longitude);

        // recuperation des données pour l'auto zoom et auto centrage
        if (lat_min > latitude) {
            lat_min = latitude;
        }
        if (lat_max < latitude) {
            lat_max = latitude;
        }
        if (lng_min > longitude) {
            lng_min = longitude;
        }
        if (lng_max < longitude) {
            lng_max = longitude;
        }

        var myLatLng = new google.maps.LatLng(value.latitude, value.longitude);
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: mapGoogle,
            icon: '/wp-content/themes/biotope/assets/images/marker.png',
            title: value.title
        });

        var infowindow = new google.maps.InfoWindow({
            content: value.content
        });

        google.maps.event.addListener(marker, 'click', function () {
            google.maps.event.trigger(mapGoogle, 'click');
            infowindow.open(mapGoogle, marker);
        });
        google.maps.event.addListener(mapGoogle, 'click', function () {
            infowindow.close();
        });
    });

    var lat_center = (parseFloat(lat_max) + parseFloat(lat_min)) / 2.0;
    var lng_center = (parseFloat(lng_max) + parseFloat(lng_min)) / 2.0;

    // Auto centrage et auto zoom
    mapGoogle.setCenter(new google.maps.LatLng(lat_center, lng_center));
    mapGoogle.fitBounds(new google.maps.LatLngBounds(
    //bottom left
    new google.maps.LatLng(lat_min, lng_min),
    //top right
    new google.maps.LatLng(lat_max, lng_max)));
}

$(document).ready(function () {
    console.log("ready  !");

    // On cache les sous-menus :
    $(".page--sidebar_nav ul.children").hide();
    // On sélectionne tous les items de liste portant la classe "toggleSubMenu"

    // et on remplace l'élément span qu'ils contiennent par un lien :
    $(".page--sidebar_nav li.page_item_has_children > a").each(function () {
        // On stocke le contenu du span :
        var TexteSpan = $(this).text();
        $(this).replaceWith('<a href="" class="parent close" title="Afficher le sous-menu">' + TexteSpan + '<\/a>');
    });

    //On ouvre le menu parent de la page en cours
    $(".page--sidebar_nav li.current_page_parent ul.children").show();
    $(".page--sidebar_nav li.current_page_parent > a").addClass('open');
    $(".page--sidebar_nav li.current_page_parent > a").removeClass('close');

    // On modifie l'évènement "click" sur les liens dans les items de liste
    // qui portent la classe "toggleSubMenu" :
    $(".page--sidebar_nav li.page_item_has_children > a").click(function () {
        // Si le sous-menu était déjà ouvert, on le referme :
        if ($(this).next("ul.children:visible").length != 0) {
            $(this).next("ul.children").slideUp("normal");
            $(this).removeClass('open');
            $(this).addClass('close');
        }
        // Si le sous-menu est caché, on ferme les autres et on l'affiche :
        else {
                $(".page--sidebar_nav ul.children").slideUp("normal");
                $(".page--sidebar_nav ul.children").prev().removeClass('open');
                $(".page--sidebar_nav ul.children").prev().addClass('close');

                $(this).next("ul.children").slideDown("normal");
                $(this).removeClass('close');
                $(this).addClass('open');
            }
        // On empêche le navigateur de suivre le lien :
        return false;
    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 14) {
            $('#header').addClass('pageIsScrolled');
            $('#header').removeClass('pageIsUnScrolled');
        } else {
            $('#header').removeClass('pageIsScrolled');
            $('#header').addClass('pageIsUnScrolled');
        }
    });
    $('html, body').animate({
        scrollTop: $('header').offset().top + 1
    }, 'fast');
    // Scollhome
    $('.scrollhome').click(function () {
        var page = $(this).attr('href');
        var speed = 750;
        $('html, body').animate({ scrollTop: $(page).offset().top - 90 }, speed);
        return false;
    });
    // Menu burger
    $('.menu-openpanel').click(function () {
        $('.bar').toggleClass('animate');
        $('.menu--overlay').toggleClass('openpanel');
        $('#header').toggleClass('is-openpanel-menu');
        $('body').toggleClass('is-menu');
        // $('.menu--overlay').fadeToggle(100, 'linear');
        $('.menu--zone-item').delay(100).toggleClass('slideDownIn');
    });
    $('.search-openpanel').click(function () {
        $('.bar').toggleClass('animate');
        $('.search--overlay').toggleClass('openpanel');
        $('#header').toggleClass('is-openpanel-search');
    });
    // sidebar
    $('.sidebar').click(function () {
        $(this).toggleClass('move');
    });

    $('#open-modal-auteur').click(function () {
        $('.icone-shoot').toggleClass('is-modal-auteur');
        $('#modal-auteur').toggleClass('is-modal-auteur');
    });
    $('#modal-auteur-close').click(function () {
        $('.icone-shoot').toggleClass('is-modal-auteur');
        $('#modal-auteur').toggleClass('is-modal-auteur');
    });

    $('.owl-zoom').owlCarousel({
        loop: true,
        margin: 10,
        nav: true,
        dots: false,
        items: 1,
        navText: ''
    });

    // ### Addthis Init
    if ($('.share-page').length > 0) {
        addthis.shareButton();
    }

    // ### Gmap init
    if ($('#map').length > 0) {
        initMap();
    }

    var os = new OnScreen({
        tolerance: 0,
        debounce: -1000,
        container: window
    });
    os.on('enter', '.home-stats', function (element, event) {
        $('.nombre').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 2000,
                easing: 'swing',
                step: function step(now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
        os.destroy();
    });

    function modalhome() {
        button.addClass("active");
    }

    $(function () {

        var $sidebar = $(".page--sidebar"),
            $window = $(window),
            offset = $sidebar.offset(),
            topPadding = 120;

        $window.scroll(function () {
            if ($window.scrollTop() > offset.top) {
                $sidebar.stop().animate({
                    marginTop: $window.scrollTop() - offset.top + topPadding
                });
            } else {
                $sidebar.stop().animate({
                    marginTop: 0
                });
            }
        });
    });

    /**
     * Google Map Info Window Customisation
     * Add a css class `.gm-style-bg` when user click on the map
     * Works with multiple classes on page
     * Dependencies: Drupal, Google Map Module
     */
    var gMap = document.querySelectorAll('[id*="map"]');
    if (gMap.length) {
        var _loop = function _loop(i) {
            var map = gMap[i];
            google.maps.event.addDomListener(map, 'click', function () {
                var currentInfoWindow = map.querySelector('.gm-style .gm-style-iw');
                if (currentInfoWindow) {
                    currentInfoWindow.parentNode.classList.add('gm-style-iw-container');
                    currentInfoWindow.previousElementSibling.classList.add('gm-style-bg');
                }
            });
        };

        for (var i = 0; i < gMap.length; i++) {
            _loop(i);
        }
    }

    $(document).ready(function () {
        var opentuile = $('.tuille--item');
        opentuile.on('click', function (e) {
            $(this).children('.tuille--popup').toggleClass('opentuile');
        });
        var closetuile = $('.tuille--popup');
        closetuile.on('click', function (e) {
            $(this).children('.tuille--popup').toggleClass('opentuile');
        });
    });
});