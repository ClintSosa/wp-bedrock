<?php
/**
 * biotope functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package biotope
 */

if ( ! function_exists( 'biotope_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function biotope_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on biotope, use a find and replace
     * to change 'biotope' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'biotope', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
        'header-menu-zone-1' => esc_html__( 'Header zone 1', 'biotope' ),
        'header-menu-zone-2' => esc_html__( 'Header zone 2', 'biotope' ),
        'header-menu-zone-3' => esc_html__( 'Header zone 3', 'biotope' ),
        'header-menu-zone-4' => esc_html__( 'Header zone 4', 'biotope' ),
        'header-menu-zone-5' => esc_html__( 'Header zone 5', 'biotope' ),
        'header-menu-zone-6' => esc_html__( 'Header zone 6', 'biotope' ),
        'header-menu-zone-7' => esc_html__( 'Header zone 7', 'biotope' ),
        'footer-menu-zone-1' => esc_html__( 'Footer zone 1', 'biotope' ),
        'footer-menu-zone-2' => esc_html__( 'Footer zone 2', 'biotope' ),
        'footer-menu-zone-3' => esc_html__( 'Footer zone 3', 'biotope' ),
        'footer-menu-zone-4' => esc_html__( 'Footer zone 4', 'biotope' ),
        'footer-menu-zone-5' => esc_html__( 'Footer zone 5', 'biotope' ),
        'footer-menu-zone-6' => esc_html__( 'Footer zone 6', 'biotope' ),
        'footer-menu-zone-7' => esc_html__( 'Footer zone 7', 'biotope' ),
      //'menu-1' => esc_html__( 'Primary', 'biotope' ),
      //'menu-2' => esc_html__( 'Footer', 'biotope' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'biotope_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    ) ) );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support( 'custom-logo', array(
      'height'      => 250,
      'width'       => 250,
      'flex-width'  => true,
      'flex-height' => true,
    ) );
  }
endif;
add_action( 'after_setup_theme', 'biotope_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function biotope_content_width() {
  $GLOBALS['content_width'] = apply_filters( 'biotope_content_width', 640 );
}
add_action( 'after_setup_theme', 'biotope_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function biotope_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__( 'Sidebar', 'biotope' ),
    'id'            => 'sidebar-1',
    'description'   => esc_html__( 'Add widgets here.', 'biotope' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
}
add_action( 'widgets_init', 'biotope_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function biotope_scripts() {
  // wp_enqueue_style( 'biotope-style', get_stylesheet_uri() );

  wp_enqueue_style( 'biotope-front-style', get_template_directory_uri() . '/assets/css/app.min.css' );

  // wp_enqueue_script( 'biotope-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

  // wp_enqueue_script( 'biotope-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }

  // wp_enqueue_script( 'biotope_map', get_template_directory_uri() . '/js/map.js', array('jquery'));

  wp_enqueue_script( 'jQuery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', array('jquery'));
  wp_enqueue_script( 'on-screen', 'https://unpkg.com/onscreen/dist/on-screen.umd.min.js', array());
  wp_enqueue_script( 'remodal', 'https://cdnjs.cloudflare.com/ajax/libs/remodal/1.1.1/remodal.min.js', array());
  wp_enqueue_script( 'biotope-front-js', get_template_directory_uri() . '/assets/js/app.js', array(), '20151215', true);

  $args = array();
  if(is_front_page() || is_home() || is_page( array( 'projets-en-cours' ))) :
    $args = array('post_type' => 'missions', 'posts_per_page' => -1, 'suppress_filters' => false);
  elseif( is_page( array( 'en-france' ) )) :
    $args = array('post_type' => 'bureaux', 'posts_per_page' => -1, 'meta_key' => 'emplacement', 'meta_value' => 'france', 'suppress_filters' => false);
  elseif( is_page( array( 'dans-le-monde' ) )) :
    $args = array('post_type' => 'bureaux', 'posts_per_page' => -1, 'meta_key' => 'emplacement', 'meta_value' => 'world', 'suppress_filters' => false);
  endif;

  $all_missions = get_posts( $args );

  $php_data = $markers = array();
  foreach($all_missions as $mission) :
    $id = $mission->ID;
    $content = '<div class="map_popup">';
      $content .= '<div class="map_thumb">'.get_the_post_thumbnail($id, array(200, 200)).'</div>';
      $content .= '<div class="map_info">';
        $content .= '<h3 class="map_info_title">'.sanitize_text_field(get_the_title($id)).'</h3>';
        $content .= '<div class="map_info_subtitle">'.get_field('zone_geographique', $id).'</div>';
        $content .= '<p class="map_info_content">'.sanitize_text_field($mission->post_content).'</p>';
      $content .= '</div>';
    $content .= '</div>';

    //preg_match("/{lat: (.*), lng: (.*)}/", get_field('pays', $id), $coordonnees);

    $markers[] = array(
      'latitude' => get_field('latitude', $id),
      'longitude' => get_field('longitude', $id),
      'title' => sanitize_text_field(get_the_title($id)),
      'icon' => '',
      'content' => $content,
    );
  endforeach;

  $php_data = array(
    'markers' => $markers,
  );

  wp_localize_script( 'biotope-front-js', 'PHP_DATA', $php_data );

  wp_enqueue_script( 'addthis', '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a2ea7546bb1e829', array('jquery'), '', true );

  wp_enqueue_script( 'googleapi', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDM7I_NmWaFyicFJZpEgrvGsT8ALV6rwi8', array('jquery'), '', true );

}
add_action( 'wp_enqueue_scripts', 'biotope_scripts' );




/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
//require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
  require get_template_directory() . '/inc/jetpack.php';
}

add_action( 'init', 'unregister_tags' );
function unregister_tags() {
  unregister_taxonomy_for_object_type( 'post_tag', 'post' );
}

// Register Missions
add_action( 'init', 'custom_missions', 0 );
function custom_missions() {
  $labels = array(
    'name'                  => _x( 'missions', 'Missions', 'biotope' ),
    'singular_name'         => _x( 'mission', 'Mission', 'biotope' ),
    'menu_name'             => __( 'Missions', 'biotope' ),
    'name_admin_bar'        => __( 'Mission', 'biotope' ),
    'archives'              => __( 'mission archivée', 'biotope' ),
    'attributes'            => __( 'mission Attributes', 'biotope' ),
    'parent_item_colon'     => __( 'Parent mission:', 'biotope' ),
    'all_items'             => __( 'Toutes les missions', 'biotope' ),
    'add_new_item'          => __( 'Ajouter une mission', 'biotope' ),
    'add_new'               => __( 'Ajouter', 'biotope' ),
    'new_item'              => __( 'Nouvelle mission', 'biotope' ),
    'edit_item'             => __( 'Edit mission', 'biotope' ),
    'update_item'           => __( 'Update mission', 'biotope' ),
    'view_item'             => __( 'View mission', 'biotope' ),
    'view_items'            => __( 'View missions', 'biotope' ),
    'search_items'          => __( 'Search mission', 'biotope' ),
    'not_found'             => __( 'Not found', 'biotope' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'biotope' ),
    'featured_image'        => __( 'Featured Image', 'biotope' ),
    'set_featured_image'    => __( 'Set featured image', 'biotope' ),
    'remove_featured_image' => __( 'Remove featured image', 'biotope' ),
    'use_featured_image'    => __( 'Use as featured image', 'biotope' ),
    'insert_into_item'      => __( 'Insert into item', 'biotope' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'biotope' ),
    'items_list'            => __( 'missions list', 'biotope' ),
    'items_list_navigation' => __( 'missions list navigation', 'biotope' ),
    'filter_items_list'     => __( 'Filter items list', 'biotope' ),
  );
  $args = array(
    'label'                 => __( 'mission', 'biotope' ),
    'description'           => __( 'Les missions Biotope', 'biotope' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
    'taxonomies'            => array(),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'             => 'dashicons-admin-site'
  );
  register_post_type( 'missions', $args );
}

// Register Bureaux
add_action( 'init', 'custom_bureaux', 0 );
function custom_bureaux() {
  $labels = array(
    'name'                  => _x( 'Bureaux', 'Bureaux', 'biotope' ),
    'singular_name'         => _x( 'Bureau', 'Bureau', 'biotope' ),
    'menu_name'             => __( 'Bureaux', 'biotope' ),
    'name_admin_bar'        => __( 'Bureau', 'biotope' ),
    'archives'              => __( 'bureau Archives', 'biotope' ),
    'attributes'            => __( 'bureau Attributes', 'biotope' ),
    'parent_item_colon'     => __( 'Parent bureau:', 'biotope' ),
    'all_items'             => __( 'Tout les bureaux', 'biotope' ),
    'add_new_item'          => __( 'Ajouter une bureau', 'biotope' ),
    'add_new'               => __( 'Ajouter', 'biotope' ),
    'new_item'              => __( 'Nouveau bureau', 'biotope' ),
    'edit_item'             => __( 'Edit bureau', 'biotope' ),
    'update_item'           => __( 'Update bureau', 'biotope' ),
    'view_item'             => __( 'View bureau', 'biotope' ),
    'view_items'            => __( 'View bureaux', 'biotope' ),
    'search_items'          => __( 'Search bureau', 'biotope' ),
    'not_found'             => __( 'Not found', 'biotope' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'biotope' ),
    'featured_image'        => __( 'Featured Image', 'biotope' ),
    'set_featured_image'    => __( 'Set featured image', 'biotope' ),
    'remove_featured_image' => __( 'Remove featured image', 'biotope' ),
    'use_featured_image'    => __( 'Use as featured image', 'biotope' ),
    'insert_into_item'      => __( 'Insert into item', 'biotope' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'biotope' ),
    'items_list'            => __( 'bureaux list', 'biotope' ),
    'items_list_navigation' => __( 'bureaux list navigation', 'biotope' ),
    'filter_items_list'     => __( 'Filter items list', 'biotope' ),
  );
  $args = array(
    'label'                 => __( 'Bureau', 'biotope' ),
    'description'           => __( 'Les bureaux Biotope', 'biotope' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
    'taxonomies'            => array(),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'             => 'dashicons-admin-home'
  );
  register_post_type( 'bureaux', $args );
}

// Register Actualités
function custom_actualites() {

  $labels = array(
    'name'                  => _x( 'News', 'Post Type General Name', 'biotope' ),
    'singular_name'         => _x( 'News', 'Post Type Singular Name', 'biotope' ),
    'menu_name'             => __( 'News', 'biotope' ),
    'name_admin_bar'        => __( 'News', 'biotope' ),
    'archives'              => __( 'Item Archives', 'biotope' ),
    'attributes'            => __( 'Item Attributes', 'biotope' ),
    'parent_item_colon'     => __( 'Parent Item:', 'biotope' ),
    'all_items'             => __( 'All Items', 'biotope' ),
    'add_new_item'          => __( 'Add New Item', 'biotope' ),
    'add_new'               => __( 'Add New', 'biotope' ),
    'new_item'              => __( 'New Item', 'biotope' ),
    'edit_item'             => __( 'Edit Item', 'biotope' ),
    'update_item'           => __( 'Update Item', 'biotope' ),
    'view_item'             => __( 'View Item', 'biotope' ),
    'view_items'            => __( 'View Items', 'biotope' ),
    'search_items'          => __( 'Search Item', 'biotope' ),
    'not_found'             => __( 'Not found', 'biotope' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'biotope' ),
    'featured_image'        => __( 'Featured Image', 'biotope' ),
    'set_featured_image'    => __( 'Set featured image', 'biotope' ),
    'remove_featured_image' => __( 'Remove featured image', 'biotope' ),
    'use_featured_image'    => __( 'Use as featured image', 'biotope' ),
    'insert_into_item'      => __( 'Insert into item', 'biotope' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'biotope' ),
    'items_list'            => __( 'Items list', 'biotope' ),
    'items_list_navigation' => __( 'Items list navigation', 'biotope' ),
    'filter_items_list'     => __( 'Filter items list', 'biotope' ),
  );
  $args = array(
    'label'                 => __( 'Actualite', 'biotope' ),
    'description'           => __( 'Les actualités Biotope', 'biotope' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor', 'thumbnail' ),
    'taxonomies'            => array( 'category' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => 'actualites',
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'             => 'dashicons-format-aside',
  );
  register_post_type( 'actualites', $args );

}
add_action( 'init', 'custom_actualites', 0 );

// Register Espace presse
function custom_espace_presse() {
  $labels = array(
    'name'                  => _x( 'Espace presse', 'Espace presse', 'biotope' ),
    'singular_name'         => _x( 'Espace presse', 'Espace presse', 'biotope' ),
    'menu_name'             => __( 'Espace presse', 'biotope' ),
    'name_admin_bar'        => __( 'Espace presse', 'biotope' ),
    'archives'              => __( 'Espaces presse Archives', 'biotope' ),
    'attributes'            => __( 'Espace presse Attributes', 'biotope' ),
    'parent_item_colon'     => __( 'Parent espace presse:', 'biotope' ),
    'all_items'             => __( 'Tous les espaces presse', 'biotope' ),
    'add_new_item'          => __( 'Ajouter nouvel espace presse', 'biotope' ),
    'add_new'               => __( 'Ajouter', 'biotope' ),
    'new_item'              => __( 'Nouvel espace presse', 'biotope' ),
    'edit_item'             => __( 'Modifier espace presse', 'biotope' ),
    'update_item'           => __( 'Update espace presse', 'biotope' ),
    'view_item'             => __( 'Voir l\'espace presse', 'biotope' ),
    'view_items'            => __( 'Voir les espaces presse', 'biotope' ),
    'search_items'          => __( 'Search Item', 'biotope' ),
    'not_found'             => __( 'Not found', 'biotope' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'biotope' ),
    'featured_image'        => __( 'Featured Image', 'biotope' ),
    'set_featured_image'    => __( 'Set featured image', 'biotope' ),
    'remove_featured_image' => __( 'Remove featured image', 'biotope' ),
    'use_featured_image'    => __( 'Use as featured image', 'biotope' ),
    'insert_into_item'      => __( 'Insert into item', 'biotope' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'biotope' ),
    'items_list'            => __( 'Items list', 'biotope' ),
    'items_list_navigation' => __( 'Items list navigation', 'biotope' ),
    'filter_items_list'     => __( 'Filter items list', 'biotope' ),
  );
  $args = array(
    'label'                 => __( 'Espace presse', 'biotope' ),
    'description'           => __( 'Espace presse', 'biotope' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor'),
    'taxonomies'            => array( '' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => 'espace-presse',
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    //'menu_icon'             => 'dashicons-format-aside',
  );
  register_post_type( 'document_presse', $args );
}
add_action( 'init', 'custom_espace_presse', 0 );

// Register Espace presse Taxonomy
function custom_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', 'biotope' ),
    'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', 'biotope' ),
    'menu_name'                  => __( 'Taxonomy', 'biotope' ),
    'all_items'                  => __( 'All Items', 'biotope' ),
    'parent_item'                => __( 'Parent Item', 'biotope' ),
    'parent_item_colon'          => __( 'Parent Item:', 'biotope' ),
    'new_item_name'              => __( 'New Item Name', 'biotope' ),
    'add_new_item'               => __( 'Add New Item', 'biotope' ),
    'edit_item'                  => __( 'Edit Item', 'biotope' ),
    'update_item'                => __( 'Update Item', 'biotope' ),
    'view_item'                  => __( 'View Item', 'biotope' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'biotope' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'biotope' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'biotope' ),
    'popular_items'              => __( 'Popular Items', 'biotope' ),
    'search_items'               => __( 'Search Items', 'biotope' ),
    'not_found'                  => __( 'Not Found', 'biotope' ),
    'no_terms'                   => __( 'No items', 'biotope' ),
    'items_list'                 => __( 'Items list', 'biotope' ),
    'items_list_navigation'      => __( 'Items list navigation', 'biotope' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    'rewrite'                    => false,
  );
  register_taxonomy( 'taxonomy', array( 'document_presse' ), $args );

}
add_action( 'init', 'custom_taxonomy', 0 );

// Register Telechargements
function custom_telechargement() {
  $labels = array(
    'name'                  => _x( 'Téléchargements', 'Documents presse', 'biotope' ),
    'singular_name'         => _x( 'Téléchargement', 'Document presse', 'biotope' ),
    'menu_name'             => __( 'Téléchargements', 'biotope' ),
    'name_admin_bar'        => __( 'Téléchargements', 'biotope' ),
    'archives'              => __( 'Téléchargements Archives', 'biotope' ),
    'attributes'            => __( 'Téléchargements Attributes', 'biotope' ),
    'parent_item_colon'     => __( 'Parent Téléchargements:', 'biotope' ),
    'all_items'             => __( 'Tous les téléchargements', 'biotope' ),
    'add_new_item'          => __( 'Ajouter nouveau téléchargement', 'biotope' ),
    'add_new'               => __( 'Ajouter', 'biotope' ),
    'new_item'              => __( 'Nouveau téléchargement', 'biotope' ),
    'edit_item'             => __( 'Modifier téléchargement', 'biotope' ),
    'update_item'           => __( 'Update téléchargement', 'biotope' ),
    'view_item'             => __( 'Voir le téléchargement', 'biotope' ),
    'view_items'            => __( 'Voir les téléchargements', 'biotope' ),
    'search_items'          => __( 'Search Item', 'biotope' ),
    'not_found'             => __( 'Not found', 'biotope' ),
    'not_found_in_trash'    => __( 'Not found in Trash', 'biotope' ),
    'featured_image'        => __( 'Featured Image', 'biotope' ),
    'set_featured_image'    => __( 'Set featured image', 'biotope' ),
    'remove_featured_image' => __( 'Remove featured image', 'biotope' ),
    'use_featured_image'    => __( 'Use as featured image', 'biotope' ),
    'insert_into_item'      => __( 'Insert into item', 'biotope' ),
    'uploaded_to_this_item' => __( 'Uploaded to this item', 'biotope' ),
    'items_list'            => __( 'Items list', 'biotope' ),
    'items_list_navigation' => __( 'Items list navigation', 'biotope' ),
    'filter_items_list'     => __( 'Filter items list', 'biotope' ),
  );
  $args = array(
    'label'                 => __( 'Téléchargement', 'biotope' ),
    'description'           => __( 'Téléchargement de Biotope', 'biotope' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'editor'),
    'taxonomies'            => array( '' ),
    'hierarchical'          => false,
    'public'                => true,
    'show_ui'               => true,
    'show_in_menu'          => true,
    'menu_position'         => 5,
    'show_in_admin_bar'     => true,
    'show_in_nav_menus'     => true,
    'can_export'            => true,
    'has_archive'           => 'telechargements',
    'exclude_from_search'   => false,
    'publicly_queryable'    => true,
    'capability_type'       => 'page',
    'menu_icon'             => 'dashicons-external',
  );
  register_post_type( 'telechargements', $args );
}
add_action( 'init', 'custom_telechargement', 0 );

// Register Telechargements Taxonomy
function telechargement_taxonomy() {

  $labels = array(
    'name'                       => _x( 'Taxonomies', 'Taxonomy General Name', 'biotope' ),
    'singular_name'              => _x( 'Taxonomy', 'Taxonomy Singular Name', 'biotope' ),
    'menu_name'                  => __( 'Taxonomy', 'biotope' ),
    'all_items'                  => __( 'All Items', 'biotope' ),
    'parent_item'                => __( 'Parent Item', 'biotope' ),
    'parent_item_colon'          => __( 'Parent Item:', 'biotope' ),
    'new_item_name'              => __( 'New Item Name', 'biotope' ),
    'add_new_item'               => __( 'Add New Item', 'biotope' ),
    'edit_item'                  => __( 'Edit Item', 'biotope' ),
    'update_item'                => __( 'Update Item', 'biotope' ),
    'view_item'                  => __( 'View Item', 'biotope' ),
    'separate_items_with_commas' => __( 'Separate items with commas', 'biotope' ),
    'add_or_remove_items'        => __( 'Add or remove items', 'biotope' ),
    'choose_from_most_used'      => __( 'Choose from the most used', 'biotope' ),
    'popular_items'              => __( 'Popular Items', 'biotope' ),
    'search_items'               => __( 'Search Items', 'biotope' ),
    'not_found'                  => __( 'Not Found', 'biotope' ),
    'no_terms'                   => __( 'No items', 'biotope' ),
    'items_list'                 => __( 'Items list', 'biotope' ),
    'items_list_navigation'      => __( 'Items list navigation', 'biotope' ),
  );
  $args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_tagcloud'              => true,
    'rewrite'                    => false,
  );
  register_taxonomy( 'telechargement_taxonomy', array( 'telechargements' ), $args );

}
add_action( 'init', 'telechargement_taxonomy', 0 );

// Init ACF
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
  if( function_exists('acf_add_options_page') ) {
    $option_page = acf_add_options_page(array(
      'page_title' => __('Choosit - Options du site', 'biotope'),
      'menu_title' => __('Choosit', 'biotope'),
      'menu_slug'  => 'theme-general-settings',
      'capability' => 'administrator',
      'icon_url'   => plugins_url('choosit_options/img/favico_choosit.png', 'choosit_options'),
      'position' => 99,
    ));
  }
}

function singledl_tolistingdl() {
    if( is_singular( 'telechargements' ) ) {
        wp_redirect( home_url( '/telechargements/' ), 301 );
        exit();
    }
    if( is_singular( 'document_presse' ) ) {
        wp_redirect( home_url( '/espace-presse/' ), 301 );
        exit();
    }
}
add_action( 'template_redirect', 'singledl_tolistingdl' );
