<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package biotope
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon/apple-touch-icon.png">
      <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon/favicon-32x32.png">
      <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon/favicon-16x16.png">
      <link rel="manifest" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon/site.webmanifest">
      <link rel="mask-icon" href="<?php bloginfo('template_directory'); ?>/assets/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="theme-color" content="#ffffff">
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(); ?>>
  <div class="menu--overlay"  grid="center justify-center">
    <div container>
        <div grid="justify-between wrap row">
          <div class="menu--zone-item">
          <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-1',
              'menu_id' => 'header-menu-zone-1',
              'menu_class' => 'menu-main'
          ));
          ?>
          </div>
          <div class="menu--zone-item">
          <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-2',
              'menu_id' => 'header-menu-zone-2',
              'menu_class' => 'menu-main'
          ));
          ?>
          </div>
          <div class="menu--zone-item">
              <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-3',
              'menu_id' => 'header-menu-zone-3',
              'menu_class' => 'menu-main'
          ));
              ?>
          </div>
          <div class="menu--zone-item">
              <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-4',
              'menu_id' => 'header-menu-zone-4',
              'menu_class' => 'menu-main'
          ));
              ?>
          </div>
          <div class="menu--zone-item">
              <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-5',
              'menu_id' => 'header-menu-zone-5',
              'menu_class' => 'menu-main'
          ));
              ?>
          </div>
          <div class="menu--zone-item">
              <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-6',
              'menu_id' => 'header-menu-zone-6',
              'menu_class' => 'menu-main'
          ));
              ?>
          </div>
          <div class="menu--zone-item">
              <?php
          wp_nav_menu(array(
              'theme_location' => 'header-menu-zone-7',
              'menu_id' => 'header-menu-zone-6',
              'menu_class' => 'menu-main'
          ));
          ?>
          </div>
          <div class="menu--zone-item">
              <?php  get_template_part('inc/social'); ?>
          </div>
        </div>
    </div>
  </div>
  <div class="search--overlay"  grid="center justify-center">
      <div container>
          <?php echo get_search_form(); ?>
      </div>
  </div>
  <header id="header" class="header" grid="center justify-between">
      <div class="menu-openpanel">
          <div class="bar"></div>
          <span>Menu</span>
      </div>
      <div class="logo">
          <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
              <?php  get_template_part('assets/images/inline', 'logo.svg'); ?>
          </a>
      </div>
      <div grid="justify-between">
          <div class="search-openpanel">
              <div class="icone-search">
                  <?php  get_template_part('assets/images/inline', 'icone-search.svg'); ?>
              </div>
              <div class="icone-search_close">
                  <div class="bar"></div>
              </div>


          </div>
          <?php do_action('wpml_add_language_selector'); ?>
      </div>


  </header>

    <div id="main">
