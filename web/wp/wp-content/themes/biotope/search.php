<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package biotope
 */

get_header(); ?>
    <section class="header-splash" style="background-image: url('<?php the_field('image_header_actualite', 'option') ?>')">

        <div container>
            <div grid="bottom">
                <h1>
                    Résultat de la recherche pour : <br />
                    <?php
                    /* translators: %s: search query. */
                    printf( esc_html__( '%s', 'biotope' ), '<span>' . get_search_query() . '</span>' );
                    ?>
                </h1>
            </div>
        </div>

    </section>

    <section>
        <div container>
            <?php print choosit_wp_breadcrumb();?>
        </div>
    </section>
    <section id="page-<?php the_ID(); ?>" class="section-actu">
        <div container>
            <div grid="">
                <div class="page--content">
                    <?php
                    if ( have_posts() ) : ?>

                        <div grid="justify-between wrap">
                            <?php
                            global $wp_query;
                            /* Start the Loop */
                            while ( have_posts() ) : the_post();
                                get_template_part( 'template-parts/list-search' );
                            endwhile; ?>
                        </div>

                        <div class="pagination">
                            <?php
                            //Pagination
                            $big = 999999999; // need an unlikely integer
                            print paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                //'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $wp_query->max_num_pages,
                                'prev_text' => __('Previous'),
                                'next_text' => __('Next'),
                            ) );
                            ?>
                        </div>
                        <?php
                    else : ?>

                        <?php get_template_part( 'template-parts/content', 'none' ); ?>

                        <?php
                    endif; ?>
                </div>

                <div class="page--sidebar">

                    <a href="<?php get_page_link(19) ?>" class="btn btn--contact">Contactez-nous</a>
                </div>

            </div>
        </div>
    </section>
<?php
get_footer();
