<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package biotope
 */

?>


    </div>
  <div id="subfooter" class="subfooter--galery">
    <?php
    //On affiche le bandeau d'image aléatoire
    $footer_images = get_images_footer_random_by_directories();
    if(!empty($footer_images)) : ?>
      <div grid="justify-between row">
        <?php
        foreach($footer_images as $footer_image) :
          $url = !empty($footer_image['url']) ? $footer_image['url'] : ''; ?>
          <div class="subfooter--galery_item" style="background-image: url('<?php print $url;?>')"></div>
          <?php
        endforeach; ?>
      </div>
      <?php
    endif;?>

  </div>

  <footer id="footer" class="footer">
    <div class="footer--info" container>
        <div grid="justify-between">
          <div class="footer--info_logo">
              <a href="<?php print esc_url(home_url('/')); ?>" rel="home">
                  <?php  get_template_part('assets/images/inline', 'logo.svg'); ?>
              </a>
          </div>
          <div class="footer--info_menumain">
              <div grid="justify-between wrap">
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-1',
                          'menu_id' => 'footer-menu-zone-1',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-2',
                          'menu_id' => 'footer-menu-zone-2',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-3',
                          'menu_id' => 'footer-menu-zone-3',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-4',
                          'menu_id' => 'footer-menu-zone-4',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-5',
                          'menu_id' => 'footer-menu-zone-5',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-6',
                          'menu_id' => 'footer-menu-zone-6',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php
                      wp_nav_menu(array(
                          'theme_location' => 'footer-menu-zone-7',
                          'menu_id' => 'footer-menu-zone-7',
                          'menu_class' => 'menu-main'
                      ));
                      ?>
                  </div>
                  <div class="menu--zone-item">
                      <?php  get_template_part('inc/social'); ?>
                  </div>
          </div>

        </div>
    </div>
      <div class="footer--end" container>
          <div grid="justify-between">
              <p>Copyright © biotope - Tous droits réservés</p>
              <ul>
                  <li>
                      <a href="#">Crédits</a>
                  </li>
                  <li>
                      <a href="#">Mentions Légales</a>
                  </li>
                    <li>
                      <a href="http://www.choosit.com" class="" target="_blank"><img src="<?php print get_template_directory_uri() ?>/assets/images/logo-choosit.png"></a>
                  </li>
              </ul>
          </div>
      </div>
  </footer>

<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>
<?php wp_footer(); ?>


<script id="__bs_script__">//<![CDATA[
    document.write("<script async src='http://HOST:8080/browser-sync/browser-sync-client.js?v=2.18.13'><\/script>".replace("HOST", location.hostname));
//]]></script>


</body>
</html>
