<div class="socials">
    <ul class="socials--list">
        <li class="socials--list_item">
            <a target="_blank" href="<?php print get_option('options_site_facebook');?>" class="facebook"><?php  get_template_part('assets/images/inline', 'icone-facebook.svg'); ?></a>

        </li>
        <li class="socials--list_item">
            <a target="_blank" href="<?php print get_option('options_site_twitter');?>" class="twitter"><?php  get_template_part('assets/images/inline', 'icone-twitter.svg'); ?></a>

        </li>
        <li class="socials--list_item">
            <a target="_blank" href="<?php print get_option('options_site_youtube');?>" class="youtube"><?php  get_template_part('assets/images/inline', 'icone-youtube.svg'); ?></a>

        </li>
    </ul>
</div>