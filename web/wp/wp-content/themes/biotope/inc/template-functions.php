<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package biotope
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function biotope_body_classes( $classes ) {
	// Adds a class of hfeed to non-singular pages.
	if ( ! is_singular() ) {
		$classes[] = 'hfeed';
	}

	return $classes;
}
add_filter( 'body_class', 'biotope_body_classes' );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function biotope_pingback_header() {
	if ( is_singular() && pings_open() ) {
		echo '<link rel="pingback" href="', esc_url( get_bloginfo( 'pingback_url' ) ), '">';
	}
}
add_action( 'wp_head', 'biotope_pingback_header' );


function hap_hide_the_archive_title( $title ) {
    // Skip if the site isn't LTR, this is visual, not functional.
    // Should try to work out an elegant solution that works for both directions.
    if ( is_rtl() ) {
        return $title;
    }
    // Split the title into parts so we can wrap them with spans.
    $title_parts = explode( ': ', $title, 2 );
    // Glue it back together again.
    if ( ! empty( $title_parts[1] ) ) {
        $title = wp_kses(
            $title_parts[1],
            array(
                'span' => array(
                    'class' => array(),
                ),
            )
        );
        $title = '<span class="screen-reader-text">' . esc_html( $title_parts[0] ) . ': </span>' . $title;
    }
    return $title;
}
add_filter( 'get_the_archive_title', 'hap_hide_the_archive_title' );