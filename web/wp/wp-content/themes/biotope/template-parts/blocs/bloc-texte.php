<section class="<?php print $layout;?> module <?php print $bloc_bg;?>">
  <div class="module__container">
    <div class="module__content">
      <div class="module__header">
        <div class="module__titre"><h2><?php print $titre; ?></h2></div>
      </div>

      <div class="module__body">
        <div class="texte"><?php print $text; ?></div>
      </div>
    </div>
  </div>
</section>
