<section class="<?php print $layout;?> module">
  <div class="module__container">
    <div class="module__content">
      <div class="module__visuel">
        <?php if(!empty($image)) : $size = 'medium'; // (thumbnail, medium, large, full or custom size) ?>
          <div class="image"><?php print wp_get_attachment_image( $image['ID'], $size ); ?></div>
        <?php endif; ?>

        <?php if(!empty($site)) : ?>
          <div class="name"><?php print $name; ?></div>
        <?php endif; ?>

        <?php if(!empty($site)) : ?>
          <div class="site">
            <a href="<?php print $site; ?>" target="_blank"><?php print $site; ?></a>
          </div>
        <?php endif; ?>
      </div>

      <div>
        <div class="module__header">
          <?php if(!empty($titre)) : ?>
            <div class="module__titre">
              <h3><?php print $titre; ?></h3>
            </div>
          <?php endif; ?>
        </div>

        <div class="module__body">
          <?php if(!empty($text)) : ?>
            <div class="text">
              <?php print $text; ?>
            </div>
          <?php endif; ?>
        </div>
      </div>

    </div>
  </div>
</section>
