<section class="<?php print $layout;?> module">
  <div class="module__container">
    <div class="module__content">
      <div class="module__body">
        <div class="texte"><?php print $text; ?></div>
      </div>
    </div>
  </div>
</section>
