<section class="<?php print $layout;?> module">
  <div class="module__container">
    <div class="module__content">

      <div class="module__heading">
        <?php if(!empty($titre)) : ?>
          <h3 class="module__title"><?php print $titre; ?></h3>
        <?php endif; ?>
      </div>

      <div class="module__body">
        <?php if(!empty($pages)) : ?>
          <?php foreach($pages as $page_data) :
            switch($page_data['type_de_page']) :
              case 'interne' :
                $page = $page_data['lien_page'][0]; ?>
                <div class="interne">

                  <div class="image">
                    <?php print get_the_post_thumbnail($page->ID, 'thumbnail');?>
                  </div>

                  <div class="titre">
                    <a href="<?php print get_permalink($page->ID);?>"><h3><?php print $page->post_title;?></h3></a>
                  </div>

                </div>
                <?php
                break;

              case 'externe' : ?>
                <div class="externe">

                  <div class="image">
                    <?php
                    $image = $page_data['image_page'];
                    $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
                    print wp_get_attachment_image( $image['ID'], $size );
                    ?>
                  </div>

                  <div class="titre">
                    <a href="<?php print $page_data['url_page'];?>" target="_blank"><h3><?php print $page_data['titre_page'];?></h3></a>
                  </div>

                </div>
                <?php
                break;

            endswitch;

          endforeach; ?>
        <?php endif; ?>
      </div>

  </div>
</section>
