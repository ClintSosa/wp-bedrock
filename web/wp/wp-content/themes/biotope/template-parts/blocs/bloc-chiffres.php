<section class="<?php print $layout;?> module">
  <div class="module__container">
    <div class="module__content">
      <div class="module__header">
        <div class="module__titre">
          <h3><?php print $titre;?></h3>
        </div>
      </div>

      <?php if(!empty($blocs)) : ?>
        <div class="module__body">

          <?php foreach($blocs as $bloc) : ?>
            <div class="bloc">
              <div class="nombre">
                <span><?php print $bloc['nombre']; ?></span>
              </div>

              <div class="text">
                <span><?php print $bloc['texte']; ?></span>
              </div>

              <div class="legende">
                <span><?php print $bloc['legende']; ?></span>
              </div>
            </div>
          <?php endforeach; ?>

        </div>
      <?php endif; ?>

    </div>
  </div>
</section>
