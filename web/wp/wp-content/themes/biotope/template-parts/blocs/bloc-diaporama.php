<?php
$has_video = '';
foreach($ressources as $ressource) :
  if($ressource['type_de_ressource'] == 'video') :
    $has_video = 'has_video';
  endif;
endforeach;
?>

<section class="<?php print $layout;?> <?php print $has_video;?> module">
  <div class="module__container">
    <div class="module__content">
      <div class="module__body">
        <?php if(!empty($ressources)) : ?>
          <ul>
            <?php foreach($ressources as $ressource) : ?>
              <li>
                <?php
                switch($ressource['type_de_ressource']) :
                  case 'image' :
                    $image = $ressource['image'];
                    $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
                    print wp_get_attachment_image( $image['ID'], $size );
                    break;

                  case 'video' :
                    $video = $ressource['video'];
                    //extraction de l'url de la balise src=""
                    preg_match('/src="(.+?)"/', $video, $matches_url );
                    $src = $matches_url[1];

                    //extraction de l'ID youtube de l'url
                    preg_match('/embed(.*?)?feature/', $src, $matches_id );
                    $youtube_id = $matches_id[1];
                    $youtube_id = str_replace( str_split( '?/' ), '', $youtube_id );
                    ?>
                    <div data-type="youtube" data-video-id="<?php print $youtube_id;?>"></div>
                    <?php
                    // @TODO A supprimer quand la librairy Plyr sera en place
                    print $video;
                    break;

                endswitch; ?>
              </li>
            <?php endforeach; ?>
          </ul>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
