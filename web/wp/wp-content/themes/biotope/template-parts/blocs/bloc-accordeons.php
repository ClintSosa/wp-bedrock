<section class="<?php print $layout;?> module">
  <div class="module__container">

    <div class="module__content accordeons">
      <div class="module__heading">
        <?php if(!empty($titre)) : ?>
          <h3 class="module__title"><?php print $titre; ?></h3>
        <?php endif; ?>
      </div>

      <div class="module__body tabs">
        <?php
        if(!empty($accordeons)) :
            $i = 1;
          foreach($accordeons as $accordeon) : ?>
            <div class="tab">
                <input id="tab-<?php echo $i; ?>" type="checkbox" name="tabs">
                <label for="tab-<?php echo $i; ?>"><?php print $accordeon['titre'];?></label>
              <div class="tab-content"><?php print $accordeon['texte'];?></div>
            </div>
            <?php
              $i++;
          endforeach;
        endif; ?>
      </div>
    </div>

  </div>
</section>
