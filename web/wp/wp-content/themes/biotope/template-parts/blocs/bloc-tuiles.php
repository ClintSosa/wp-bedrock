<section class="<?php print $layout;?> module">
  <div class="module__container">
    <div class="module__content">
        <?php if(!empty($titre)) : ?>
            <h2><?php print $titre; ?></h2>
        <?php endif; ?>
        <?php if(!empty($text)) : ?>
            <p><?php print $text; ?></p>
        <?php endif; ?>
      <div class="module__body">
        <?php if(!empty($elements)) : ?>
            <?php foreach($elements as $element) : ?>
                <?php
                $image = $element['image'];
                $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)

                ?>
              <div class="tuille--item">
                  <div class="tuille--item_bg" style="background-image: url('<?php print wp_get_attachment_url( $image['ID'], $size ); ?>')">

                  </div>
                  <div class="tuille--item_titre">
                      <h3><?php print $element['titre'];?></h3>
                      <div class="icone-more"></div>
                  </div>

                <div class="tuille--popup">
                    <div class="tuille--popup_inner">
                        <div class="icone-close"></div>
                        <h3><?php print $element['titre'];?></h3>
                        <p><?php print $element['text'];?></p>
                    </div>

                </div>
              </div>
            <?php endforeach; ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
