<section class="<?php print $layout;?> module visuel-<?php print $alignement;?>">
  <div class="module__container">
    <div class="module__content">
      <div class="module__visuel">
        <?php
        $size = 'medium'; // (thumbnail, medium, large, full or custom size)
        print wp_get_attachment_image( $image['ID'], $size ); ?>
      </div>


      <div class="module__header">
        <?php if(!empty($titre)) : ?>
          <div class="module__titre">
            <h3><?php print $titre; ?></h3>
          </div>
        <?php endif; ?>
      </div>

      <div class="module__body">
        <?php if(!empty($text)) : ?>
          <div class="text">
            <?php print $text; ?>
          </div>
        <?php endif; ?>
      </div>

    </div>
  </div>
</section>
