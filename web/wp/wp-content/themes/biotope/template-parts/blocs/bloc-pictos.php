<section class="<?php print $layout;?> module">
  <div class="module__container">
    <div class="module__content">

      <div class="module__header">
        <div class="module__titre">
          <h3><?php print $titre;?></h3>
        </div>
      </div>

      <div class="module__body">
        <?php if(!empty($blocs)) : ?>
          <?php foreach($blocs as $bloc) : ?>
            <div class="bloc">
              <div class="picto">
                <?php
                $size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
                print wp_get_attachment_image( $bloc['picto']['ID'], $size );
                ?>
              </div>

              <div class="text">
                <span><?php print $bloc['texte']; ?></span>
              </div>

              <div class="legende">
                <span><?php print $bloc['legende']; ?></span>
              </div>
            </div>
          <?php endforeach; ?>
        <?php endif; ?>
      </div>

  </div>
</section>
