<div class="item">
  <?php $image = get_field('visuel', $child->ID);
  if(!empty($image)) :
    //size : (thumbnail, medium, large, full or custom size)
    $size = 'full'; ?>
    <div class="visuel">
      <?php print wp_get_attachment_image($image['ID'], $size); ?>
    </div>
    <?php
  endif; ?>
    <div class="content">
  <?php if(!empty($child->post_title)) : ?>
    <h2><?php print $child->post_title;?></h2>
  <?php endif; ?>

  <?php if(!empty(get_field('chapo', $child->ID))) : ?>

      <p><?php print get_field('chapo', $child->ID); ?></p>

  <?php endif; ?>

  <?php if(!empty($child->ID)) : ?>
    <a class="more" href="<?php print get_permalink($child->ID);?>"><?php _e('En savoir plus');?></a>
  <?php endif; ?>
    </div>
</div>
