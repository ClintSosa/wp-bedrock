<section style="background-image: url('<?php the_post_thumbnail_url(); ?>')" class="header-splash"></section>


<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div container>
        <div grid="">
            <div class="post--content">
                <?php print choosit_wp_breadcrumb();?>
                <h1 class="title"><?php the_title(); ?></h1>
                <div class="date"><?php print get_the_date('d/m/Y'); ?></div>
                <?php the_content(); ?>
            </div>
            <div class="post--sidebar">
                <div class="post--sidebar_nav" grid="justify-between">
                    <div>
                        <a href="#"><?php  get_template_part('assets/images/inline', 'icone-prev.svg'); ?></a>
                        <a href="#"><?php  get_template_part('assets/images/inline', 'icone-next.svg'); ?></a>
                    </div>
                    <div>
                        <a href="#"><?php  get_template_part('assets/images/inline', 'icone-nav.svg'); ?></a>
                    </div>
                </div>
                <div class="post--sidebar_share">
                    <h4>Partager l’article :</h4>
                </div>
            </div>
        </div>
    </div>
</section>

