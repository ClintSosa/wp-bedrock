<?php
$args = array(
  'posts_per_page'   => -1, // Pas de pagniation pour avoir tous les résultats
  'orderby'          => 'date',
  'order'            => 'DESC',
  'post_type'        => 'actualites',
  'post_status'      => 'publish',
  //'exclude'          => get_the_ID(),
  'suppress_filters' => false,
);
$all_posts = get_posts( $args );

$prev = $next = false;
foreach($all_posts as $index => $item) :
    if($item->ID == $post->ID) :
      if(isset($all_posts[$index - 1])) :
        $prec_post = $all_posts[$index - 1];
        $prec_url = get_permalink($prec_post->ID);
        $prev = true;
      endif;
      if(isset($all_posts[$index + 1])) :
        $next_post = $all_posts[$index + 1];
        $next_url = get_permalink($next_post->ID);
        $next = true;
      endif;
      break;
    endif;
endforeach;
?>

<section style="background-image: url('<?php the_field('image_header_actualite', 'option'); ?>')" class="header-splash"></section>

<section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div container>
      <div grid="">
          <div class="post--content">
              <?php print choosit_wp_breadcrumb();?>
              <h1 class="title"><?php the_title(); ?></h1>
              <div class="date"><?php print get_the_date('d/m/Y'); ?></div>

              <div class="thumbnail">
                  <?php the_post_thumbnail('full') ?>
              </div>
              <?php the_content(); ?>


          </div>
          <div class="post--sidebar">
              <div class="post--sidebar_nav" grid="justify-between">
                  <div>
                    <?php if($prev) : ?>
                      <a class="prev" title="prev" href="<?php print $prec_url;?>"><?php get_template_part('assets/images/inline', 'icone-prev.svg');?></a>
                    <?php endif; ?>
                    <?php if($next) : ?>
                      <a class="next" title="next" href="<?php print $next_url;?>"><?php get_template_part('assets/images/inline', 'icone-next.svg');?></a>
                    <?php endif; ?>
                  </div>
                  <div>
                      <a href="<?php print get_post_type_archive_link(get_post_type());?>"><?php  get_template_part('assets/images/inline', 'icone-nav.svg'); ?></a>
                  </div>
              </div>
              <div class="post--sidebar_share">

                <!-- AddThis Button BEGIN -->
                <h4><?php print __('Share article', 'biotope'); ?> :</h4>

                <div class="share-page">

                  <a class="addthis_button_facebook">
                    <span class="addthis_share_button" data-service="facebook" data-url="<?php print get_permalink();?>" data-title="<?php print get_the_title();?>" data-media="" data-description=""><img src="<?php echo get_template_directory_uri() ?>/assets/images/icone-share_fb.svg"></span>
                  </a>
                  <a class="addthis_button_twitter">
                    <span class="addthis_share_button" data-service="twitter" data-url="<?php print get_permalink();?>" data-title="<?php print get_the_title();?>" data-media="" data-description=""><img src="<?php echo get_template_directory_uri() ?>/assets/images/icone-share_tw.svg"></span>
                  </a>
                  <a class="addthis_button_linkedin">
                    <span class="addthis_share_button" data-service="linkedin" data-url="<?php print get_permalink();?>" data-title="<?php print get_the_title();?>" data-media="" data-description=""><img src="<?php echo get_template_directory_uri() ?>/assets/images/icone-share_in.svg"></span>
                  </a>
                  <a class="addthis_button_google_plusone_share">
                    <span class="addthis_share_button" data-service="googleplus" data-url="<?php print get_permalink();?>" data-title="<?php print get_the_title();?>" data-media="" data-description=""><img src="<?php echo get_template_directory_uri() ?>/assets/images/icone-share_gplus.svg"></span>
                  </a>

                </div>

              </div>
          </div>
      </div>
  </div>
</section>


<div class="more-actualites">
    <div container>
        <div grid="justify-between">
          <h2><?php print __('To read', 'biotope');?></h2>
            <a class="more" href="<?php echo get_post_type_archive_link('actualites'); ?>"><?php print __('All news','biotope'); ?></a>
        </div>
        <div class="home-actualites-loop" grid="justify-between">
            <?php
            $args = $args = array(
                'posts_per_page'   => 4,
                'orderby'          => 'date',
                'order'            => 'DESC',
                'post_type'        => 'actualites',
                'exclude'          => get_the_ID(),
                'post_status'      => 'publish',
                'suppress_filters' => false,
            );
            $all_actu = get_posts($args);
            $size = 'thumbnail';
            foreach ($all_actu as $actu) : ?>
                <div class="item">
                    <a href="<?php print get_permalink($actu->ID);?>">
                        <?php $featured_img_url = get_the_post_thumbnail_url($actu->ID, $size) ?>
                        <div class="post-thumbnail" style="background-image: url('<?php echo $featured_img_url ?>')"></div>
                        <div class="item-post">
                            <h3 class="title"><?php print $actu->post_title; ?></h3>
                            <span class="date"><?php print get_the_date( 'd/m/Y', $actu->ID); ?></span>
                        </div>

                    </a>
                </div>
                <?php
            endforeach; ?>
        </div>
    </div>
</div>
