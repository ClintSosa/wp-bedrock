<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <a href="<?php print get_permalink();?>">
      <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'thumbnail') ?>
      <div class="post-thumbnail" style="background-image: url('<?php echo $featured_img_url ?>')"></div>
      <div class="post-inner">
          <h3 class="title"><?php print get_the_title(); ?></h3>
          <span class="date"><?php print get_the_date('d/m/Y'); ?></span>
      </div>

  </a>
</div>
