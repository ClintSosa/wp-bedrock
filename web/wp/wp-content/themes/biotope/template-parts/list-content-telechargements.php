<?php
$terms = wp_get_post_terms(get_the_ID(),'telechargement_taxonomy');
if(!empty($terms)) :
    $term_post = array();
    foreach($terms as $term) :
        $term_post[] = $term->name;
    endforeach;
endif; ?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div grid="justify-between">

        <div class="dl-content">
            <?php if(!empty($term_post)) : ?>
              <div class="suttitre">
                  <span class="categorie"><?php print implode(', ', $term_post);?></span>
                  <span class="date"><?php print get_the_date('d/m/Y'); ?></span>
              </div>
            <?php endif; ?>

            <h3 class="title"><?php print get_the_title(); ?></h3>
            <?php the_content();?>
        </div>

        <div class="dl-file">
            <?php
            $file = get_field('fichier');
            if(!empty($file)) :
                $extension = pathinfo($file['filename'], PATHINFO_EXTENSION);
                $filesize = filesize( get_attached_file( $file['ID'] ) );
                $filesize = size_format($filesize, 2);
                ?>
                <div class="dl-file-icone">
                    <a href="<?php print $file['url'];?>" download><?php  get_template_part('assets/images/inline', 'icone-dl.svg'); ?></a>
                    <span><?php print $extension;?> - <?php print $filesize;?></span>
                </div>
            <?php endif; ?>
        </div>
    </div>

</div>
