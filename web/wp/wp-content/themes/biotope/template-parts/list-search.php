<div id="post-<?php the_ID(); ?>" <?php post_class('search-item'); ?>>
  <a href="<?php print get_permalink();?>">
      <div class="post-inner">
          <h3 class="title"><?php print get_the_title(); ?></h3>
          <?php the_excerpt() ?>
      </div>

  </a>
</div>
