<?php
/**
 * Template part for displaying a message that posts cannot be found
 */
?>

<h3 class="title"><?php print __( 'No result found.', 'biotope' ); ?></h3>
