<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package biotope
 */

get_header(); ?>

    <section class="header-splash" style="background-image: url('<?php the_field('image_header_actualite', 'option') ?>')">

        <div container>
            <div grid="bottom">
                <h1><?php esc_html_e( 'Page 404', 'biotope' ); ?></h1>
            </div>
        </div>

    </section>

    <section>
        <div container>
            <?php print choosit_wp_breadcrumb();?>
        </div>
    </section>



    <section class="error-404 not-found">
        <div container>
            <h2><?php esc_html_e( 'Désolé, page non trouvée', 'biotope' ); ?></h2>
            <p><?php esc_html_e( 'La page à laquelle vous tentez d’accéder n’est plus disponible.', 'biotope' ); ?></p>
        </div>
    </section><!-- .error-404 -->


<?php
get_footer();
