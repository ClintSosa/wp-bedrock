<?php
/**
 * Template Name: Page sommaire Visuels
 */
?>

<?php get_header(); ?>


    <?php
    while ( have_posts() ) : the_post();
      $post_id = $post->ID;
    endwhile; // End of the loop.
    ?>

    <?php $image = get_field('visuel', $post_id);
    if(!empty($image)) :
        //size : (thumbnail, medium, large, full or custom size)
        $size = 'full'; ?>

        <section class="header-splash" style="background-image: url('<?php print wp_get_attachment_url($image['ID'], $size); ?>')">

        </section>

        <?php
    endif; ?>

    <section class="metiers-chapo">
        <div container>
            <h1><?php print get_the_title($post_id);?></h1>
            <p><?php print get_field('chapo', $post_id); ?></p>
        </div>
    </section>

    <section class="metiers-list">
    <div container>
        <div grid="justify-center wrap">
            <?php
            $args = array(
                'post_parent' => $post_id,
                'post_type'   => $post->post_type,
                'numberposts' => -1,
                'post_status' => 'publish',
                'order' => 'ASC',
            );
            $childrens = get_children( $args );

            if($childrens) :
                foreach($childrens as $children) :
                    choosit_get_template_part('template-parts/summary', 'visuel', array('child' => $children));
                endforeach;
            endif;
            ?>
        </div>
    </div>
    </section>



<?php
get_footer();
