<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
	<input type="text" class="inputsearch" value="<?php the_search_query(); ?>" name="s" id="s" placeholder="Votre recherche" />
    <button type="submit" id="searchsubmit"><img src="<?php echo get_template_directory_uri() ?>/assets/images/icone-arrow-blue.svg"></button>
</form>