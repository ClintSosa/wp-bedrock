<?php
/**
 * Template Name: Page sommaire Pictos
 *
 */
?>
<?php get_header(); ?>


<?php
while ( have_posts() ) : the_post();
    $post_id = $post->ID;
endwhile; // End of the loop.

$args = array(
    'post_parent' => $post_id,
    'post_type'   => $post->post_type,
    'numberposts' => -1,
    'post_status' => 'publish',
    'order' => 'ASC',
);
$childrens = get_children( $args );
?>

<?php $image = get_field('visuel', $post_id);
if(!empty($image)) :
    //size : (thumbnail, medium, large, full or custom size)
    $size = 'full'; ?>

    <section class="header-splash" style="background-image: url('<?php print wp_get_attachment_url($image['ID'], $size); ?>')">

    </section>

    <?php
endif; ?>

    <section class="metiers-chapo">
        <div container>
            <h1><?php print get_the_title($post_id);?></h1>
            <p><?php print get_field('chapo', $post_id); ?></p>
        </div>
    </section>

    <section class="metiers-list metiers-list--picto">
        <div container>
            <div grid="justify-start wrap">
                <div class="list-item">
                  <?php if($childrens) : ?>
                    <?php foreach(array_values($childrens) as $i => $children) : ?>
                      <?php if($i%3 == 0) : ?>
                        <div class="list-item-inner">
                      <?php endif; ?>

                        <?php choosit_get_template_part('template-parts/summary', 'picto', array('child' => $children)); ?>

                      <?php if($i%3 == 2) : ?>
                        </div><hr />
                      <?php endif; ?>
                    <?php endforeach; ?>
                  <?php else : ?>

                    <div class="list-item-inner">
                      <p><?php print __('No children\'s page');?></p>
                    </div>

                  <?php endif; ?>
                </div>
            </div>
        </div>
    </section>


<?php
get_footer();
