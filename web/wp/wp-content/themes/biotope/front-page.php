<?php get_header(); ?>



    <div id="modal-auteur" class="auteur-info">
        <div class="auteur-info-inner">
            <div id="modal-auteur-close" class="auteur-info-close"></div>
            <h3><?php _e('Une image, un auteur', 'biotope');?></h2></h3>
            <div class="auteur-info-citation"><blockquote><?php print get_field('legende_visuel_top');?></blockquote></div>
            <div grid>
                <?php if(!empty(get_field('portrait_top')) || !empty(get_field('nom_top')) || !empty(get_field('fonction_top'))) : ?>

                    <div class="portrait">
                        <?php
                        $visu = get_field('portrait_top');
                        if(!empty($visu)) :
                            $size = 'full'; ?>
                            <div class="photo">
                                <?php print wp_get_attachment_image($visu['ID'], $size); ?>
                            </div>
                            <?php
                        endif;

                        if(!empty(get_field('nom_top'))) : ?>
                            <div class="nom"><?php print get_field('nom_top');?></div>
                            <?php
                        endif;

                        if(!empty(get_field('nom_top'))) : ?>
                            <div class="metier"><?php print get_field('fonction_top');?></div>
                            <?php
                        endif;?>
                    </div>

                <?php endif;?>

                <?php if(!empty(get_field('citation_top'))) : ?>
                    <div class="citation"><?php print get_field('citation_top');?></div>
                <?php endif;?>

            </div>
        </div>


    </div>


<?php
$imagetop = get_field('visuel_top');
$style_background = '';
if (!empty($imagetop)) :
    $size = 'large';
    $style_background = 'style="background-image : url('.$imagetop['sizes'][$size].');"';
endif; ?>



    <section <?php print $style_background;?> class="home-header-splash">


        <div id="open-modal-auteur">
            <span class="icone-shoot"></span>
        </div>
        <a class="scrollhome" href="#homecontent"><img src="<?php echo get_template_directory_uri() ?>/assets/images/mouse_scroll.svg"></a>
    </section>




    <!-- Pages misent en avant -->
    <?php if(!empty(get_field('pages'))) : ?>
      <div class="home-push">
          <div grid="justify-between">


            <?php
            $pages = get_field('pages');
            $size = 'large';
            $style_background = $style_background_icone = '';
            foreach($pages as $page) :
              if(!empty($page['visuel']['sizes'][$size])) :
                $style_background = 'style="background-image : url('.$page['visuel']['sizes'][$size].');"';
              endif;
              if(!empty($page['icone']['sizes'][$size])) :
                $style_background_icone = $page['icone']['sizes'][$size];
              endif;
              ?>
              <div class="home-push-item">
                  <div class="home-push-item_bg" <?php print $style_background;?>>

                  </div>
                <?php if(!empty($page['lien'])) : ?>
                  <a href="<?php print $page['lien'];?>">
                <?php endif; ?>

                    <span class="icon">
                        <img src="<?php print $style_background_icone;?>">
                    </span>

                    <h3><?php print $page['titre'];?></h3>

                <?php if(!empty($page['lien'])) : ?>
                  </a>
                <?php endif; ?>
              </div>
              <?php
            endforeach; ?>
          </div>
      </div>
    <?php endif; ?>

    <div id="homecontent" class="home-texte">
        <div container>
            <div grid="justify-between">
                <div column="6">
                    <h2><?php print get_field('titre');?></h2>
                </div>
                <div column="6">
                    <p><?php print get_field('texte');?></p>
                    <a href="<?php print get_field('lien');?>" class="more">En savoir plus</a>
                </div>
            </div>

        </div>
    </div>


    <div class="home-actualites">
        <div container>
            <div grid="justify-between">
                <h2><?php print __('News', 'biotope');?></h2>
                <a class="more" href="<?php echo get_post_type_archive_link('actualites'); ?>"><?php print __('All news','biotope'); ?></a>
            </div>
              <div class="home-actualites-loop" grid="justify-between">
                  <?php
                  $args = $args = array(
                    'posts_per_page'   => 4,
                    'orderby'          => 'date',
                    'order'            => 'DESC',
                    'post_type'        => 'actualites',
                    'post_status'      => 'publish',
                    'suppress_filters' => false,
                  );
                  $all_actu = get_posts($args);
                  $size = 'thumbnail';
                  foreach ($all_actu as $actu) : ?>
                    <div class="item">
                      <a href="<?php print get_permalink($actu->ID);?>">
                          <?php $featured_img_url = get_the_post_thumbnail_url($actu->ID, $size) ?>
                          <div class="post-thumbnail" style="background-image: url('<?php echo $featured_img_url ?>')"></div>
                          <div class="item-post">
                              <h3 class="title"><?php print $actu->post_title; ?></h3>
                              <span class="date"><?php print get_the_date( 'd/m/Y', $actu->ID); ?></span>
                          </div>

                      </a>
                    </div>
                    <?php
                  endforeach; ?>
              </div>
        </div>
    </div>

    <?php
    //Chiffres
    $size = 'large';
    $imagechiffres = get_field('visuel_chiffres');
    $style_background = '';
    if (!empty($imagechiffres)) :
      $style_background = 'style="background-image : url('.$imagechiffres['sizes'][$size].');"';
    endif; ?>
    <div <?php print $style_background;?> class="home-stats-bg"></div>
    <div class="home-stats">
        <div></div>
      <?php
      $blocs = get_field('blocs_chiffres');
      if(!empty($blocs)) : ?>
        <div class="home-stats-content" container>
            <div grid="justify-between">
              <?php
              foreach($blocs as $bloc) : ?>
                <div class="home-stats-content_item">
                  <span class="nombre"><?php print $bloc['chiffre'];?></span>
                  <br>
                  <span class="texte"><?php print $bloc['texte'];?></span>
                </div>
                <?php
              endforeach; ?>
            </div>
        </div>
        <?php
      endif; ?>
      <div class="home-stats-legende">
          <div container>
              <p><?php print get_field('legende_chiffre');?></p>
          </div>
      </div>
    </div>


    <div class="home-map">
        <div container>
            <div column="6">
                <h2><?php print __('Some actions in progress in the world', 'biotope');?> : </h2>
            </div>
        </div>
      <div id="map"></div>
    </div>


    <div class="home-zoom">
        <div container>
            <h2><?php _e('Zoom sur', 'biotope');?></h2>
            <div grid="justify-center">
            <div column="8">
            <div class="owl-carousel owl-theme owl-zoom">
              <?php
              if (have_rows('elements')) :
                $cpt = 1;
                while (have_rows('elements')) : the_row();
                  $image = get_sub_field('image'); ?>
                  <div id="<?php print $cpt; ?>_home-zoom-slide" class="home-zoom-slide">
                        <div class="visuel" style="background-image: url('<?php print $image['url'];?>')"></div>
                        <div class="data">
                          <span class="nbr">0<?php print $cpt; ?></span>
                          <span class="surtitre"><?php print the_sub_field('surtitre');?></span>
                          <h3><?php print the_sub_field('titre');?></h3>
                          <div><?php print the_sub_field('accroche');?></div>
                          <a href="#<?php print $cpt; ?>_home-zoom_modal" class="more"><?php print _e('En savoir plus', 'biotope');?></a>
                        </div>
                  </div>


                  <?php
                  $cpt++;
                endwhile;
              endif;
              wp_reset_postdata(); ?>
            </div>
            </div>
            </div>
        </div>
    </div>
    <?php
    if (have_rows('elements')) :
        $cpt = 1;
        while (have_rows('elements')) : the_row(); ?>

            <div class="remodal remodal-zoom" data-remodal-id="<?php print $cpt; ?>_home-zoom_modal">
                <button data-remodal-action="close" class="remodal-close"></button>
                <h2><?php print the_sub_field('titre');?></h2>
                <?php print the_sub_field('description');?>
            </div>
                <?php
                $cpt++;
            endwhile;
        endif;
    wp_reset_postdata(); ?>



<?php
get_footer();
