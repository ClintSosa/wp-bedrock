<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package biotope
 */
get_header(); ?>



    <section class="header-splash" style="background-image: url('<?php the_field('image_header_telechargement', 'option') ?>')">

        <div container>
            <div grid="bottom">
                <h1><?php the_archive_title('', false); ?> </h1>
            </div>
        </div>


    </section>

    <section>
        <div container>
            <?php print choosit_wp_breadcrumb();?>
        </div>
    </section>

    <section id="page-<?php the_ID(); ?>" class="section-dl">
        <div container>
            <div grid="">
                <div class="page--content">

                    <?php
                    if ( have_posts() ) : ?>
                      <div class="filtres">
                        <?php
                        $terms = get_terms( 'taxonomy', array('hide_empty' => true) );
                        if(!empty($terms)) :
                          $trouve = FALSE;
                          if(!empty($_GET['taxonomy'])) :
                            foreach($terms as &$term) :
                              if($term->slug == $_GET['taxonomy']) :
                                $trouve = TRUE;
                                $term->active = TRUE;
                                break;
                              endif;
                            endforeach;
                          endif;?>

                          <ul>
                            <li><a class="<?php print !$trouve ? 'active' : ''; ?>" href="<?php print get_site_url(null, 'espace-presse');?>"><?php print __('Tout afficher');?></a></li>
                            <?php
                            foreach($terms as &$term) : ?>
                              <li><a class="<?php print $term->active && $trouve ? 'active' : ''; ?>" href="<?php print get_site_url(null, 'espace-presse');?>?taxonomy=<?php print $term->slug;?>"><?php print $term->name;?></a></li>
                            <?php
                            endforeach; ?>
                          </ul>
                          <?php
                        endif; ?>
                      </div>

                      <?php
                        global $wp_query;
                        /* Start the Loop */
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/list-content', get_post_type() );
                        endwhile; ?>

                        <div class="pagination">
                            <?php
                            //Pagination
                            $big = 999999999; // need an unlikely integer
                            print paginate_links( array(
                                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                                //'format' => '?paged=%#%',
                                'current' => max( 1, get_query_var('paged') ),
                                'total' => $wp_query->max_num_pages,
                                'prev_text' => __('Previous'),
                                'next_text' => __('Next'),
                            ) );
                            ?>
                        </div>
                        <?php
                    else :
                        get_template_part( 'template-parts/content', 'none' );
                    endif; ?>
                </div>

                <div class="page--sidebar">

                    <div class="page--sidebar_nav">
                        <?php wp_nav_menu(array(
                            'menu' => '7',
                            'theme_location' => 'menu-1',
                            'menu_id' => 'sidebar-menu',
                            'menu_class' => 'menu-options-list'
                        )); ?>
                    </div>
                    <a href="<?php print get_page_link(19) ?>" class="btn btn--contact">Contactez-nous</a>
                </div>

            </div>
        </div>
    </section>

<?php
get_footer();
