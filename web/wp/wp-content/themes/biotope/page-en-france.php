<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package biotope
 */

get_header(); ?>


<?php
while ( have_posts() ) : the_post();
  $post_id = $post->ID;

  // If the post has ancestors, get its ultimate parent and make that the top post
  if ($post->post_parent && $post->ancestors) {
    $grand_parent_page_id = end($post->ancestors);
  }
  /* Liste des pages enfants pour la page courante */
  $submenu = wp_list_pages( array(
    'child_of' => $grand_parent_page_id,
    'depth' => 2,
    'sort_order' => 'asc',
    'sort_column' => 'menu_order',
    'title_li' => '',
    'echo' => false,
  ) );

endwhile; ?>

<?php
$image = get_field('visuel', $post_id);
if(!empty($image)) : $size = 'full'; ?>
  <section class="header-splash" style="background-image: url('<?php print wp_get_attachment_url($image['ID'], $size); ?>')">

    <div container>
      <div grid="bottom">
        <h1><?php print get_the_title($post_id);?></h1>
      </div>
    </div>

  </section>
  <?php
endif; ?>

<section>
  <div container>
    <?php print choosit_wp_breadcrumb();?>
  </div>
</section>

<section id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div container>
    <div grid="">
      <div class="page--content">

		  <?php
		  // check if the flexible content field has rows of data
		  if (have_rows('blocs')) :
			  while (have_rows('blocs')) : the_row();
				  switch (get_row_layout()) :

					  case 'bloc_texte':
						  $variables = array(
							  'post_id' => $post->ID,
							  'layout' => get_row_layout(),
							  'titre' => get_sub_field('titre'),
							  'text' => get_sub_field('text'),
						  );
						  choosit_get_template_part('template-parts/blocs/bloc', 'texte', $variables);
						  break;

				  endswitch;
			  endwhile;
		  endif;
		  ?>

        <div class="home-map">
          <div id="map"></div>
        </div>

        <?php
        // check if the flexible content field has rows of data
        if (have_rows('blocs')) :
          while (have_rows('blocs')) : the_row();
            //echo "<strong>" . get_row_layout() . "</strong>";
            switch (get_row_layout()) :

              case 'bloc_accordeons':
                $variables = array(
                  'post_id' => $post->ID,
                  'layout' => get_row_layout(),
                  'titre' => get_sub_field('titre'),
                  'accordeons' => get_sub_field('accordeons'),
                );
                choosit_get_template_part('template-parts/blocs/bloc', 'accordeons', $variables);
                break;

            endswitch;
          endwhile;
        endif;
        ?>

        <?php $ressources = get_field('ressources', $post_id);
        if(!empty($ressources)) : ?>
          <section class="bloc_ressources module">
            <div class="module__container">
              <div class="module__content">
                <h3><?php _e('Ressources', 'biotope');?></h3>

                <div class="module__body">
                  <?php foreach($ressources as $ressource) :
                    $title = $ressource['titre'];
                    $file = $ressource['document'];
                    $extension = pathinfo($file['filename'], PATHINFO_EXTENSION);
                    $filesize = filesize( get_attached_file( $file['ID'] ) );
                    $filesize = size_format($filesize, 2);
                    ?>
                      <a href="<?php print $file['url'];?>" target="_blank">
                          <div class="document">
                              <span class="titre"><?php print $title;?></span>
                              <span class="seize">(<?php print $extension;?> - <?php print $filesize;?>)</span>

                          </div>
                          <span class="icone-dl"><?php  get_template_part('assets/images/inline', 'icone-dl.svg'); ?></span>
                      </a>
                  <?php endforeach; ?>
                </div>

              </div>
            </div>
          </section>
          <?php
        endif; ?>
      </div>

      <div class="page--sidebar">

        <?php if($submenu) : ?>
          <div class="page--sidebar_nav">
              <ul class="menu-options-list">
                  <?php print $submenu;?>
              </ul>
          </div>
        <?php endif; ?>
          <a href="<?php print get_page_link(19) ?>" class="btn btn--contact">Contactez-nous</a>
      </div>

    </div>
  </div>
</section>


<?php
get_footer();
